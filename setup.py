from setuptools import setup, find_packages


setup(
    name='payments',
    version=__import__('payments').VERSION,
    url='https://bitbucket.org/ffitlv/payments',
    author='Mārtiņš Šulcs',
    author_email='shulcsm@gmail.com',
    packages=find_packages(exclude=['tests*']),
    zip_safe=False,
    include_package_data=True,
    license='MIT',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.6',
        'Topic :: Software Development :: Libraries :: Application Frameworks',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    python_requires='>=3.6',
    install_requires=[
        'Django',
        'requests>=2',
        'responses>=0.10',
        'django-enumfields>=2.0.0',
        'marshmallow==3.0.0rc3',
        'psycopg2-binary',  # JSONField

        # paypal_express
        'paypalrestsdk==2.0.0rc2',

        # swedbank_hps
        'dataclasses>=0.6',
        'lxml>=4.2'
    ]
)
