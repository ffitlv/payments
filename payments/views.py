from django.db.transaction import atomic
from django.views.decorators.csrf import csrf_exempt

from payments.manager import manager


@atomic
@csrf_exempt
def callback(request, provider_id, token=None, action=None):
    return manager.callback(request, provider_id, token, action)

