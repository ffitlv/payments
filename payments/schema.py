from marshmallow import Schema, fields, validate


class UserDetailSchema(Schema):
    first_name = fields.String()
    last_name = fields.String()
    phone = fields.String()
    language_code = fields.String(required=True, validate=validate.Length(equal=2))  # @TODO lower
    email = fields.Email()


class AddressSchema(Schema):
    country_code = fields.String(required=True, validate=validate.Length(equal=2))  # @TODO upper
    line_1 = fields.String(required=True)
    line_2 = fields.String(allow_none=True)

    title = fields.String(allow_none=True)
    first_name = fields.String(allow_none=True)
    last_name = fields.String(allow_none=True)

    city = fields.String(allow_none=True)
    postal_code = fields.String(allow_none=True)
    state = fields.String(allow_none=True)
    phone = fields.String(allow_none=True)


class ItemSchema(Schema):
    name = fields.String(required=True)
    quantity = fields.Integer(required=True, validate=validate.Range(min=1))
    price = fields.Decimal(required=True, as_string=True)
    description = fields.String(required=False, allow_none=True)


class PurchaseSchema(Schema):
    merchant_reference = fields.String(required=True)
    provider = fields.String(required=True)
    amount = fields.Decimal(required=True, as_string=True)
    currency = fields.String(required=True, validate=validate.Length(equal=3))  # @TODO upper
    description = fields.String(required=True)

    items = fields.Nested(ItemSchema, many=True, allow_none=True)

    subtotal = fields.Decimal(as_string=True, allow_none=True)
    tax = fields.Decimal(as_string=True, allow_none=True)
    shipping = fields.Decimal(as_string=True, allow_none=True)

    user_details = fields.Nested(UserDetailSchema, required=True, allow_none=False)
    billing_address = fields.Nested(AddressSchema, allow_none=True)
    shipping_address = fields.Nested(AddressSchema, allow_none=True)
