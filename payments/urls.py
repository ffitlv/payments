from django.conf.urls import url
from .views import callback

app_name = 'payments'

urlpatterns = [
    url(r'^callback/(?P<provider_id>[\w_]+)/(?P<token>[0-9a-zA-Z]{30})/(?:(?P<action>[\w_]+)/)?$',
        callback, name='callback'),
    url(r'^callback/(?P<provider_id>[\w_]+)/$', callback, name='callback'),
]