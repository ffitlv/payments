from decimal import Decimal

from payments.dto import Purchase, Address, UserDetails, Item

purchase = Purchase(
    merchant_reference='1',
    provider='some_provider',
    amount=Decimal('2.64'),
    currency='EUR',
    description='Test purchase',
    items=[
        Item(name='Product A', quantity=1, price=Decimal('1.01')),
        Item(name='Product B', quantity=2, price=Decimal('0.50')),
    ],
    shipping=Decimal('0.33'),
    subtotal=Decimal('2.01'),
    tax=Decimal('0.12'),
    user_details=UserDetails(
        first_name="Kārlis",
        last_name="Krūms",
        phone="77777777",
        language_code="lv",
        email="karlis@krums.lv"
    ),
    billing_address=Address(
        country_code='LV',
        line_1='Iela 123',
        line_2='Dzīvoklis 1',
        title='Mr',
        first_name='Jānis',
        last_name='Koks',
        city='Rīga',
        postal_code='LV-2121',
        state=None,
        phone='99999999'
    ),
    shipping_address=Address(
        country_code='RU',
        line_1='Iela 321',
        line_2='Dzīvoklis 2',
        title='Mrs',
        first_name='Ansis',
        last_name='Zars',
        city='Maskava',
        postal_code='101000',
        state=None,
        phone='888888888'
    )
)
