import json
from decimal import Decimal
from typing import Dict
from unittest import mock

from django.test import SimpleTestCase, TestCase
from pathlib import Path

from paypalrestsdk.v1.payments import SaleRefundRequest

from payments.const import Status, RefundResult
from payments.models import Transaction, Frame
from payments.providers.paypal import _map_error, PaypalException, AlreadyPaidException, InstrumentDeclinedException, \
    ExpressProvider, find_sale

from ..data import purchase

p = ExpressProvider()


def data(file: str) -> Dict:
    with open(Path(__file__).parents[0] / f'data/{file}.json') as f:
        return json.load(f)


class ProviderTestCase(SimpleTestCase):
    def test_create_setup_request(self):
        token = "TOKEN".ljust(30, "0")
        transaction = Transaction(token=token)
        request = p.create_setup_request(purchase, transaction)

        # @TODO test dict


class RefundTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        # Mock full cycle
        cls.t = Transaction.objects.create(
            merchant_reference='1',
            provider_reference='PAY-1B56960729604235TKQQIYVY',
            provider=p.id,
            status=Status.CONFIRMED,
            amount=Decimal('30.11'),
            purchase_data={}
        )

        Frame.objects.create(
            transaction=cls.t,
            request=data('create_payment_request'),
            response=data('create_payment_response')
        )

        Frame.objects.create(
            transaction=cls.t,
            request=data('execute_payment_request'),
            response=data('execute_payment_response')
        )

    def test_refund_full(self):
        query_response = mock.Mock()
        query_response.frame_data = data('execute_payment_response')

        refund_response = mock.Mock()
        refund_response.frame_data = data('refund_response')

        with mock.patch.object(ExpressProvider, 'request', side_effect=[query_response, refund_response]) as do_request:
            self.assertEqual(p.refund(self.t, Decimal('30.11')), RefundResult.SUCCESS)

            ((refund_request,), _) = do_request.call_args

        self.assertIsInstance(refund_request, SaleRefundRequest)
        self.assertTrue("4XP56210M0797192Y" in refund_request.path)
        self.assertDictEqual(refund_request.body, {})

        # refund frame persisted
        frame = self.t.frames.last()
        self.assertDictEqual(frame.request, {})
        self.assertDictEqual(frame.response, refund_response.frame_data)

    def test_refund_partial(self):
        query_response = mock.Mock()
        query_response.frame_data = data('execute_payment_response')

        refund_response = mock.Mock()
        refund_response.frame_data = data('refund_response')

        with mock.patch.object(ExpressProvider, 'request', side_effect=[query_response, refund_response]) as do_request:
            self.assertEqual(p.refund(self.t, Decimal('10.11')), RefundResult.SUCCESS)

            ((refund_request,), _) = do_request.call_args

        self.assertIsInstance(refund_request, SaleRefundRequest)
        self.assertTrue("4XP56210M0797192Y" in refund_request.path)

        request_payload = {
            "amount": {
                "total": "10.11",
                "currency": "USD"
            },
        }

        self.assertDictEqual(refund_request.body, request_payload)

        # refund frame persisted
        frame = self.t.frames.last()
        self.assertDictEqual(frame.request, request_payload)
        self.assertDictEqual(frame.response, refund_response.frame_data)


class MiscTestCase(SimpleTestCase):
    def test_find_sale(self):
        response = data('execute_payment_response')
        self.assertDictEqual(
            find_sale(response),
            {
                "id": "4XP56210M0797192Y",
                "create_time": "2014-09-22T23:22:27Z",
                "update_time": "2014-09-22T23:31:13Z",
                "amount": {
                    "total": "30.11",
                    "currency": "USD"
                },
                "payment_mode": "INSTANT_TRANSFER",
                "state": "completed",
                "protection_eligibility": "ELIGIBLE",
                "protection_eligibility_type": "ITEM_NOT_RECEIVED_ELIGIBLE",
                "transaction_fee": {
                    "value": "1.75",
                    "currency": "USD"
                },
                "parent_payment": "PAY-4N746561P0587231SKQQK6MY",
                "links": [
                    {
                        "href": "https://api.paypal.com/v1/payments/sale/4XP56210M0797192Y",
                        "rel": "self",
                        "method": "GET"
                    },
                    {
                        "href": "https://api.paypal.com/v1/payments/sale/4XP56210M0797192Y/refund",
                        "rel": "refund",
                        "method": "POST"
                    },
                    {
                        "href": "https://api.paypal.com/v1/payments/payment/PAY-4N746561P0587231SKQQK6MY",
                        "rel": "parent_payment",
                        "method": "GET"
                    }
                ]
            })

    def test_map_error(self):
        with self.assertRaises(PaypalException):
            _map_error({})

        with self.assertRaises(AlreadyPaidException):
            _map_error({'name': 'PAYMENT_ALREADY_DONE'})

        with self.assertRaises(InstrumentDeclinedException):
            _map_error({'name': 'INSTRUMENT_DECLINED'})
