from decimal import Decimal
from unittest import mock

from django.http import HttpRequest, HttpResponse
from django.test import TestCase

from payments.const import Status, Result, RefundResult
from payments.dto import Purchase
from payments.exceptions import StatusError, InvalidAmountError, Error
from payments.handler import BaseHandler
from payments.manager import Manager
from payments.models import Transaction, Frame
from payments.providers.base import ProviderBase
from .data import purchase as test_purchase


class ManagerTestCase(TestCase):
    def test_setup_failure(self):
        class FailureProvider(ProviderBase):
            id = "some_provider"

            def setup(self, transaction: Transaction, purchase: Purchase, request: HttpRequest) -> str:
                Frame.objects.create(transaction=transaction)
                raise Error

        m = Manager({'some_provider': FailureProvider()}, BaseHandler())

        with self.assertRaises(Error):
            m.setup(test_purchase, HttpRequest())

        self.assertFalse(Frame.objects.all().exists())
        self.assertFalse(Transaction.objects.all().exists())

    @mock.patch.multiple('payments.models.Transaction', is_refundable=mock.DEFAULT, refundable_amount=mock.DEFAULT,
                         new_callable=mock.PropertyMock)
    def test_refund_input(self, is_refundable, refundable_amount):
        p = ProviderBase()
        m = Manager({'dummy': p}, BaseHandler())

        t = Transaction(provider='dummy')
        is_refundable.return_value = False

        with self.assertRaises(StatusError):
            m.refund(t)

        is_refundable.return_value = True
        refundable_amount.return_value = Decimal('1')

        with self.assertRaises(InvalidAmountError):
            m.refund(t, Decimal(2))

    def test_refund_success(self):
        p = ProviderBase()
        m = Manager({'dummy': p}, BaseHandler())
        t = Transaction(provider='dummy', status=Status.CONFIRMED, amount=Decimal(1))
        t.save = mock.MagicMock()

        p.refund = mock.MagicMock(return_value=RefundResult.SUCCESS)
        m.refund(t, Decimal('0.3'))
        p.refund.assert_called_once_with(t, Decimal('0.3'))

        self.assertEqual(t.status, Status.REFUNDED)
        self.assertEqual(t.refunded_amount, Decimal('0.3'))
        t.save.assert_called_once()

    def test_stale_callback(self):
        response = HttpResponse()

        h = BaseHandler()
        h.handle_result = mock.MagicMock()
        h.handle_response = mock.MagicMock(return_value=response)

        m = Manager({'dummy': ProviderBase()}, h)

        t = Transaction.objects.create(
            merchant_reference='1',
            provider_reference='1',
            provider='dummy',
            status=Status.CONFIRMED,
            amount=Decimal(1),
            purchase_data={}
        )
        req = HttpRequest()

        self.assertEqual(m.callback(req, 'dummy', t.token), response)
        h.handle_result.assert_called_once_with(Result.STALE_CALLBACK, t)
        h.handle_response.assert_called_once_with(req, Result.STALE_CALLBACK, t)

