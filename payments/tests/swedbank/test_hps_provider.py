from datetime import datetime
from decimal import Decimal
from unittest.mock import patch

from django.test import TestCase, RequestFactory
from django.urls import reverse

from payments.models import Transaction
from payments.providers.swedbank import HPSProvider
from payments.tests.swedbank.helper import load_xml
from ..data import purchase
from payments import dto
from ...const import Result, Status

p = HPSProvider()


class HPSProviderTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.transaction = Transaction.objects.create(
            token="TOKEN".ljust(30, "0"),
            created_at=datetime(2020, 2, 4, 14, 30),
            amount=Decimal("1"),
            purchase_data={},
        )

    def test_create_setup_request(self):
        request = p.create_setup_request(purchase, self.transaction, "127.0.0.1")
        self.maxDiff = None
        self.assertXMLEqual(
            request.dump_xml(xml_declaration=False, pretty_print=True, encoding=str),
            load_xml("setup_request_1"),
        )

    def test_create_setup_request_minimal(self):
        # We are testing if empty values are coerced
        request = p.create_setup_request(
            dto.Purchase(
                merchant_reference="1",
                provider="some_provider",
                amount=Decimal("2.64"),
                currency="EUR",
                description="Test purchase",
                user_details=dto.UserDetails(language_code="lv"),
                billing_address=dto.Address(
                    country_code="LV",
                    line_1="Iela 123",
                ),
                shipping_address=dto.Address(
                    country_code="RU",
                    line_1="Iela 321",
                ),
            ),
            self.transaction,
            "127.0.0.1",
        )
        self.maxDiff = None
        self.assertXMLEqual(
            request.dump_xml(xml_declaration=False, pretty_print=True, encoding=str),
            load_xml("setup_request_2"),
        )

    def test_process(self):
        rf = RequestFactory()

        request = rf.get(
            reverse(
                "payments:callback",
                kwargs={"provider_id": "swedbank_hps", "token": self.transaction.token},
            )
        )

        with patch(
            "payments.providers.swedbank.HPSProvider.query", return_value=Result.SUCCESS
        ):
            self.assertEqual(p.process(self.transaction, request), Result.SUCCESS)
            self.assertEqual(self.transaction.status, Status.CREATED)

        with patch(
            "payments.providers.swedbank.HPSProvider.query", return_value=Result.FAILURE
        ):
            self.assertEqual(p.process(self.transaction, request), Result.FAILURE)
            self.assertEqual(self.transaction.status, Status.CREATED)

        # unfinished bails
        with patch(
            "payments.providers.swedbank.HPSProvider.query",
            return_value=Result.UNFINISHED,
        ):
            self.assertEqual(p.process(self.transaction, request), Result.FAILURE)
            self.assertEqual(self.transaction.status, Status.REJECTED)
