from pathlib import Path


def load_xml(file: str) -> str:
    with open(Path(__file__).parents[0] / f'data/{file}.xml', 'r') as f:
        return f.read()
