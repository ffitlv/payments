from datetime import datetime
from decimal import Decimal

from django.test import SimpleTestCase

from payments.providers.swedbank import ReturnCode
from payments.providers.swedbank.dto import request, setup, response
from payments.tests.swedbank.helper import load_xml


class RequestDTOTestCase(SimpleTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.maxDiff = None
        cls.auth = request.Authentication("PASSWORD", "CLIENT")

    def test_query_request(self):
        r = request.QueryRequest(self.auth, "4500204380168644")
        self.assertXMLEqual(
            r.dump_xml(xml_declaration=False, pretty_print=True, encoding=str),
            load_xml("query_request"),
        )
        self.assertDictEqual(
            r.dump(),
            {
                "authentication": {"password": "********", "client": "********"},
                "transaction": {"method": "query", "reference": "4500204380168644"},
            },
        )

    def test_auth_request(self):
        # auth request looks like query, but response is different
        r = request.AuthRequest(self.auth, "4500204380168644")
        self.assertXMLEqual(
            r.dump_xml(xml_declaration=False, pretty_print=True, encoding=str),
            load_xml("query_request"),
        )
        self.assertDictEqual(
            r.dump(),
            {
                "authentication": {"password": "********", "client": "********"},
                "transaction": {"method": "query", "reference": "4500204380168644"},
            },
        )

    def test_cancel_request(self):
        r = request.CancelRequest(self.auth, "4500204380168644")
        self.assertXMLEqual(
            r.dump_xml(xml_declaration=False, pretty_print=True, encoding=str),
            load_xml("cancel_request"),
        )
        self.assertDictEqual(
            r.dump(),
            {
                "authentication": {"password": "********", "client": "********"},
                "transaction": {"method": "cancel", "reference": "4500204380168644"},
            },
        )

    def test_refund_request(self):
        r = request.RefundRequest(self.auth, "4500204380168644", Decimal("3.21"))
        self.assertXMLEqual(
            r.dump_xml(xml_declaration=False, pretty_print=True, encoding=str),
            load_xml("refund_request"),
        )

        self.assertDictEqual(
            r.dump(),
            {
                "authentication": {"password": "********", "client": "********"},
                "transaction": {
                    "method": "txn_refund",
                    "reference": "4500204380168644",
                    "amount": "3.21",
                },
            },
        )

    def test_setup_request(self):
        r = request.SetupRequest(
            self.auth,
            transaction=setup.SetupTransaction(
                details=setup.Details(
                    merchant_reference="OrderNumber001/1",
                    amount=Decimal("100.00"),
                    currency="EUR",
                    purchase_datetime=datetime(2015, 6, 23, 9, 0, 0),
                    merchant_url="http://shop.example.com",
                    purchase_desc="Product Description",
                    risk=setup.risk.Risk(
                        customer_details=setup.risk.CustomerDetails(
                            billing_details=setup.risk.BillingDetails(
                                name="Cardholder Name",
                                address_line1="2000 Purchase Street",
                                address_line2=None,
                                city="Washington",
                                zip_code="DC 20500",
                                country="US",
                                state_province="DC",
                            ),
                            personal_details=setup.risk.PersonalDetails(
                                first_name="Cardholder",
                                surname="Name",
                                telephone="00 123 456 789",
                            ),
                            shipping_details=setup.risk.ShippingDetails(
                                title="Mr",
                                first_name="Cardholder",
                                surname="Name",
                                address_line1="Landsvagen 40",
                                address_line2=None,
                                city="Stockholm",
                                country="SE",
                                zip_code="105 34",
                            ),
                            ip_address="8.8.8.8",
                            email_address="e-mail@email.com",
                        )
                    ),
                ),
                hosted_page_details=setup.HostedPageDetails(
                    page_set_id=2322,
                    return_url="http://shop.example.com/return.html",
                    expiry_url="https://shop.example.com/expired.html",
                    error_url="https://shop.example.com/error.html",
                    go_back_url="http://shop.example.com/goback.html",
                ),
            ),
        )
        self.assertXMLEqual(
            r.dump_xml(
                xml_declaration=False,
                pretty_print=True,
                encoding=str,
            ),
            load_xml("setup_request"),
        )

        self.assertDictEqual(
            r.dump(),
            {
                "authentication": {"password": "********", "client": "********"},
                "transaction": {
                    "details": {
                        "merchant_reference": "OrderNumber001/1",
                        "amount": "100.00",
                        "currency": "EUR",
                        "merchant_url": "http://shop.example.com",
                        "purchase_datetime": "2015-06-23T09:00:00",
                        "purchase_desc": "Product Description",
                        "risk": {
                            "channel": "W",
                            "merchant_location": None,
                            "action": "1",
                            "customer_details": {
                                "email_address": "e-mail@email.com",
                                "ip_address": "8.8.8.8",
                                "billing_details": {
                                    "name": "Cardholder Name",
                                    "address_line1": "2000 Purchase Street",
                                    "address_line2": None,
                                    "city": "Washington",
                                    "country": "US",
                                    "zip_code": "DC 20500",
                                    "state_province": "DC",
                                },
                                "personal_details": {
                                    "first_name": "Cardholder",
                                    "surname": "Name",
                                    "telephone": "00 123 456 789",
                                    "date_of_birth": None,
                                },
                                "shipping_details": {
                                    "title": "Mr",
                                    "first_name": "Cardholder",
                                    "surname": "Name",
                                    "address_line1": "Landsvagen 40",
                                    "address_line2": None,
                                    "city": "Stockholm",
                                    "country": "SE",
                                    "zip_code": "105 34",
                                },
                            },
                        },
                    },
                    "hosted_page_details": {
                        "page_set_id": 2322,
                        "return_url": "http://shop.example.com/return.html",
                        "expiry_url": "https://shop.example.com/expired.html",
                        "error_url": "https://shop.example.com/error.html",
                        "go_back_url": "http://shop.example.com/goback.html",
                        "show_card_holder_name": False,
                    },
                },
            },
        )


class ResponseDTOTestCase(SimpleTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.maxDiff = None

    def test_setup_response_serialization(self):
        resp = response.SetupResponse.load_xml(load_xml("setup_response"))
        self.assertIsInstance(resp, response.SetupResponse)
        self.assertEqual(resp.mode, "LIVE")
        self.assertEqual(resp.reason, "ACCEPTED")
        self.assertIsInstance(resp.status, ReturnCode)
        self.assertEqual(resp.status, 1)
        self.assertEqual(resp.time, 1434617190)

        self.assertIsInstance(resp.host_page_details, response.HostedPageDetails)

        resp.status.raise_for_status()
        self.assertEqual(
            resp.host_page_details.redirect_url,
            "https://accreditation.datacash.com/hps-acq_a/?HPS_SessionID=838a3964-8931-4bd1-baf4-35d7f504faaa",
        )

        self.assertDictEqual(
            resp.dump(),
            {
                "mode": "LIVE",
                "reason": "ACCEPTED",
                "status": 1,
                "time": 1434617190,
                "datacash_reference": "4300204380114157",
                "merchantreference": "OrderNumber001/01",
                "host_page_details": {
                    "hps_url": "https://accreditation.datacash.com/hps-acq_a/",
                    "session_id": "838a3964-8931-4bd1-baf4-35d7f504faaa",
                },
            },
        )

    def test_query_response_serialization(self):
        resp = response.QueryResponse.load_xml(load_xml("query_response"))
        self.assertIsInstance(resp, response.QueryResponse)
        self.assertEqual(resp.mode, "LIVE")
        self.assertEqual(resp.reason, "ACCEPTED")
        self.assertIsInstance(resp.status, ReturnCode)
        self.assertEqual(resp.status, 1)
        self.assertEqual(resp.time, 1434622712)
        self.assertEqual(resp.datacash_reference, "4300204380114157")
        self.assertEqual(
            resp.information,
            "You have queried a Full-HPS transaction, where the payment was successfully collected",
        )
        self.assertEqual(resp.merchantreference, "ffs8888985789xcvcxv")
        (attempt,) = resp.auth_attempts

        self.assertIsInstance(attempt, response.AuthAttempt)
        self.assertEqual(attempt.datacash_reference, "4500204380168644")
        self.assertEqual(attempt.dc_response, 1)
        self.assertEqual(attempt.reason, "ACCEPTED")

        self.assertDictEqual(
            resp.dump(),
            {
                "mode": "LIVE",
                "reason": "ACCEPTED",
                "status": 1,
                "time": 1434622712,
                "datacash_reference": "4300204380114157",
                "merchantreference": "ffs8888985789xcvcxv",
                "information": "You have queried a Full-HPS transaction, where the payment was successfully collected",
                "auth_attempts": [
                    {
                        "datacash_reference": "4500204380168644",
                        "dc_response": 1,
                        "reason": "ACCEPTED",
                    }
                ],
            },
        )

    def test_query_response_awaiting_acs(self):
        resp = response.QueryResponse.load_xml(load_xml("query_response_awaiting_acs"))
        self.assertIsInstance(resp, response.QueryResponse)
        self.assertEqual(resp.mode, "LIVE")
        self.assertEqual(
            resp.reason, "HPS: Payment in flight. Awaiting customer ACS submission."
        )
        self.assertIsInstance(resp.status, ReturnCode)
        self.assertEqual(resp.status, 824)
        self.assertEqual(resp.time, 1434622712)
        self.assertEqual(resp.datacash_reference, "3400112517241881")
        self.assertEqual(
            resp.information,
            "You have queried a Full-HPS transaction, where the customer has been sent for ACS validation",
        )
        self.assertEqual(resp.merchantreference, "000000049657/302")
        (attempt,) = resp.auth_attempts

        self.assertIsInstance(attempt, response.AuthAttempt)

        self.assertEqual(attempt.datacash_reference, "3400112517252515")
        self.assertEqual(attempt.dc_response, 150)
        self.assertEqual(attempt.reason, "3DS Payer Verification Required")

        self.assertDictEqual(
            resp.dump(),
            {
                "mode": "LIVE",
                "reason": "HPS: Payment in flight. Awaiting customer ACS submission.",
                "status": 824,
                "time": 1434622712,
                "datacash_reference": "3400112517241881",
                "merchantreference": "000000049657/302",
                "information": "You have queried a Full-HPS transaction, where the customer has been sent for ACS validation",
                "auth_attempts": [
                    {
                        "datacash_reference": "3400112517252515",
                        "dc_response": 150,
                        "reason": "3DS Payer Verification Required",
                    }
                ],
            },
        )

    def test_auth_response(self):
        resp = response.AuthResponse.load_xml(load_xml("auth_response"))
        self.assertIsInstance(resp, response.AuthResponse)

        self.assertEqual(resp.mode, "LIVE")
        self.assertEqual(resp.reason, "ACCEPTED")
        self.assertIsInstance(resp.status, ReturnCode)
        self.assertEqual(resp.status, 1)
        self.assertEqual(resp.time, 1434631003)

        tr = resp.transaction_result
        self.assertIsInstance(tr, response.TransactionResult)
        self.assertEqual(tr.acquirer, "Swedbank")
        self.assertEqual(tr.authcode, "790138")
        self.assertEqual(tr.datacash_reference, "4500204380168644")
        self.assertEqual(tr.environment, "ecomm")
        self.assertEqual(tr.fulfill_date, datetime(2015, 6, 18, 11, 17, 26))
        self.assertEqual(tr.fulfill_timestamp, 1434622646)
        self.assertEqual(tr.merchant_reference, "ffs8888985789xcvcxv")
        self.assertEqual(tr.reason, "ACCEPTED")
        self.assertEqual(tr.sent, "Settled")
        self.assertEqual(tr.status, "1")

        self.assertEqual(tr.transaction_date, datetime(2015, 6, 18, 11, 17, 26))
        self.assertEqual(tr.transaction_timestamp, 1434622646)

        card = tr.card
        self.assertIsInstance(card, response.Card)
        self.assertEqual(card.cv2avs_status, "ALL MATCH")
        self.assertEqual(card.card_category, "Personal")
        self.assertEqual(card.expirydate, "01/20")
        self.assertEqual(card.country, "lva")
        self.assertEqual(card.issuer, "Swedbank")
        self.assertEqual(card.pan, "444433******1111")
        self.assertEqual(card.scheme, "VISA")
        self.assertEqual(card.token, "A3C03727818940BEE7D1ABC6204AE45767828D8D")

        risk = resp.risk
        self.assertIsInstance(risk, response.Risk)
        screening_response = risk.screening_response
        self.assertIsInstance(screening_response, response.RiskResponse)
        self.assertEqual(screening_response.response_code, "00")
        self.assertEqual(screening_response.response_message, "Transaction Approved")
        self.assertEqual(screening_response.transaction_id, "4500204380168644")

        # additional_messages?

        # bankresult_response?

        self.assertDictEqual(
            resp.dump(),
            {
                "mode": "LIVE",
                "reason": "ACCEPTED",
                "status": 1,
                "time": 1434631003,
                "transaction_result": {
                    "card": {
                        "cv2avs_status": "ALL MATCH",
                        "card_category": "Personal",
                        "country": "lva",
                        "expirydate": "01/20",
                        "issuer": "Swedbank",
                        "pan": "444433******1111",
                        "scheme": "VISA",
                        "token": "A3C03727818940BEE7D1ABC6204AE45767828D8D",
                    },
                    "acquirer": "Swedbank",
                    "authcode": "790138",
                    "datacash_reference": "4500204380168644",
                    "environment": "ecomm",
                    "fulfill_date": "2015-06-18T11:17:26",
                    "fulfill_timestamp": 1434622646,
                    "merchant_reference": "ffs8888985789xcvcxv",
                    "reason": "ACCEPTED",
                    "sent": "Settled",
                    "status": "1",
                    "transaction_date": "2015-06-18T11:17:26",
                    "transaction_timestamp": 1434622646,
                },
                "risk": {
                    "screening_response": {
                        "response_code": "00",
                        "response_message": "Transaction Approved",
                        "transaction_id": "4500204380168644",
                    }
                },
            },
        )

    def test_auth_response_no_screening(self):
        resp = response.AuthResponse.load_xml(load_xml("auth_response_no_screening"))
        self.assertIsInstance(resp, response.AuthResponse)

        self.assertEqual(resp.mode, "LIVE")
        self.assertEqual(resp.reason, "ACCEPTED")
        self.assertIsInstance(resp.status, ReturnCode)
        self.assertEqual(resp.status, 1)
        self.assertEqual(resp.time, 1434631003)

        tr = resp.transaction_result
        self.assertIsInstance(tr, response.TransactionResult)
        self.assertEqual(tr.acquirer, "Swedbank")
        self.assertEqual(tr.authcode, "790138")
        self.assertEqual(tr.datacash_reference, "4500204380168644")
        self.assertEqual(tr.environment, "ecomm")
        self.assertEqual(tr.fulfill_date, datetime(2015, 6, 18, 11, 17, 26))
        self.assertEqual(tr.fulfill_timestamp, 1434622646)
        self.assertEqual(tr.merchant_reference, "ffs8888985789xcvcxv")
        self.assertEqual(tr.reason, "ACCEPTED")
        self.assertEqual(tr.sent, "Settled")
        self.assertEqual(tr.status, "1")

        self.assertEqual(tr.transaction_date, datetime(2015, 6, 18, 11, 17, 26))
        self.assertEqual(tr.transaction_timestamp, 1434622646)

        card = tr.card
        self.assertIsInstance(card, response.Card)
        self.assertEqual(card.cv2avs_status, "ALL MATCH")
        self.assertEqual(card.card_category, "Personal")
        self.assertEqual(card.expirydate, "01/20")
        self.assertEqual(card.country, "lva")
        self.assertEqual(card.issuer, "Swedbank")
        self.assertEqual(card.pan, "444433******1111")
        self.assertEqual(card.scheme, "VISA")
        self.assertEqual(card.token, "A3C03727818940BEE7D1ABC6204AE45767828D8D")

        risk = resp.risk
        self.assertIsInstance(risk, response.Risk)
        self.assertIsNone(risk.screening_response)

        self.assertDictEqual(
            resp.dump(),
            {
                "mode": "LIVE",
                "reason": "ACCEPTED",
                "status": 1,
                "time": 1434631003,
                "transaction_result": {
                    "card": {
                        "cv2avs_status": "ALL MATCH",
                        "card_category": "Personal",
                        "country": "lva",
                        "expirydate": "01/20",
                        "issuer": "Swedbank",
                        "pan": "444433******1111",
                        "scheme": "VISA",
                        "token": "A3C03727818940BEE7D1ABC6204AE45767828D8D",
                    },
                    "acquirer": "Swedbank",
                    "authcode": "790138",
                    "datacash_reference": "4500204380168644",
                    "environment": "ecomm",
                    "fulfill_date": "2015-06-18T11:17:26",
                    "fulfill_timestamp": 1434622646,
                    "merchant_reference": "ffs8888985789xcvcxv",
                    "reason": "ACCEPTED",
                    "sent": "Settled",
                    "status": "1",
                    "transaction_date": "2015-06-18T11:17:26",
                    "transaction_timestamp": 1434622646,
                },
                "risk": {"screening_response": None},
            },
        )

    def test_refund_response(self):
        resp = response.RefundResponse.load_xml(load_xml("refund_response"))
        self.assertIsInstance(resp, response.RefundResponse)

        self.assertEqual(resp.mode, "LIVE")
        self.assertEqual(resp.reason, "ACCEPTED")
        self.assertIsInstance(resp.status, ReturnCode)
        self.assertEqual(resp.status, 1)
        self.assertEqual(resp.time, 1437652436)

        self.assertEqual(resp.authcode, "REFUND ACCEPTED")
        self.assertEqual(resp.acquirer, "Swedbank")
        self.assertEqual(resp.datacash_reference, "4000204394824466")
        self.assertEqual(resp.merchantreference, "4100204394824376")
        self.assertEqual(resp.mid, "74587458")

        self.assertDictEqual(
            resp.dump(),
            {
                "mode": "LIVE",
                "reason": "ACCEPTED",
                "status": 1,
                "time": 1437652436,
                "authcode": "REFUND ACCEPTED",
                "acquirer": "Swedbank",
                "datacash_reference": "4000204394824466",
                "merchantreference": "4100204394824376",
                "mid": "74587458",
            },
        )

        """
        @TODO
        This is what we get from test env 
        <Response version='2'>
          <MAC>
            <outcome>ACCEPT</outcome>
          </MAC>
          <acquirer>Swedbank Baltic Latvia</acquirer>
          <datacash_reference>3500900025181315</datacash_reference>
          <merchantreference>3900900025181276</merchantreference>
          <mid>0007250216</mid>
          <mode>LIVE</mode>
          <reason>ACCEPTED</reason>
          <status>1</status>
          <time>1549450550</time>
        </Response>
        """
        """
        @TODO one of failures
        <Response version='2'>
            <datacash_reference>3800900025181328</datacash_reference>
            <merchantreference>3900900025181276</merchantreference>
            <mode>LIVE</mode>
            <reason>157.30 &gt; remaining funds 0.00</reason>
            <status>34</status>
            <time>1549450744</time>
        </Response>
        """

    # def test_cancel_response(self):
    #     pass
