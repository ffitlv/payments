from decimal import Decimal

from django.http import HttpRequest
from django.test import SimpleTestCase

from payments.providers.base import ProviderBase
from payments.schema import PurchaseSchema
from .data import purchase

provider = ProviderBase()
provider.id = "base"


class ProviderBaseTestCase(SimpleTestCase):
    def test_get_create_transaction_kwargs(self):
        self.maxDiff = None
        t = provider.get_create_transaction_kwargs(purchase, HttpRequest())
        self.assertDictEqual(t, {
            'merchant_reference': '1',
            'provider': 'base',
            'amount': Decimal('2.64'),
            'purchase_data':  PurchaseSchema().dump(purchase)
        })

