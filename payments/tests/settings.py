PAYMENT_BASE_PATH = 'http://payment.test'

PAYMENT_HANDLER = 'payments.handler.BaseHandler'

PAYMENT_PROVIDERS = [
    'payments.providers.paypal.ExpressProvider',
    'payments.providers.swedbank.HPSProvider'
]

PAYMENT_PAYPAL_EXPRESS_CLIENT = None
PAYMENT_PAYPAL_EXPRESS_SECRET = None
PAYMENT_PAYPAL_EXPIRIENCE_PROFILE_ID = None

PAYMENT_SWEDBANK_HPS_CLIENT = "00000000"
PAYMENT_SWEDBANK_HPS_PASSWORD = "p4SsW0rd"

PAYMENT_FIRSTDATA_PAYEEZY_CLIENT = 'https://client'
PAYMENT_FIRSTDATA_PAYEEZY_MERCHANT = 'https://merchant'
PAYMENT_FIRSTDATA_PAYEEZY_CERT = (
    'cert.pem',
    'key.pem'
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'payments_tests',
        'USER': 'postgres'
    }
}


INSTALLED_APPS = [
    # 'django.contrib.contenttypes',
    # 'django.contrib.sites',
    # 'django.contrib.auth',
    # 'django.contrib.admin',

    'payments',
]

SECRET_KEY = 'payments'
ROOT_URLCONF = 'payments.tests.urls'

