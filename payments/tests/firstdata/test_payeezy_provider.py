from datetime import datetime
from decimal import Decimal
from unittest import mock

from django.http import HttpRequest
from django.test import TestCase

from payments.models import Transaction
from payments.providers.firstdata import PayeezyProvider
from ..data import purchase


class PayeezyProviderTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

    def test_setup(self):
        p = PayeezyProvider()

        transaction = Transaction.objects.create(
            token="TOKEN".ljust(30, "0"),
            created_at=datetime(2020, 2, 4, 14, 30),
            amount=Decimal("1"),
            purchase_data={}
        )
        request = HttpRequest()

        p.merchant.start_sms = mock.MagicMock(return_value=({}, {"TRANSACTION_ID": "TEST_ID"}))
        return_url = p.setup(transaction, purchase, request)
        self.assertEqual(transaction.provider_reference, "TEST_ID")
        # @TODO test frame, return url
