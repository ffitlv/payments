from enum import Enum


class Status(Enum):
    # we do this so we can reference concrete transaction/attempt
    CREATED = 'CREATED'  # no provider refernece, provider doesn't know about transaction yet
    PENDING = 'PENDING'  # has provider reference

    # finished
    CONFIRMED = 'CONFIRMED'
    REJECTED = 'REJECTED'
    CANCELED = 'CANCELED'
    TIMEOUT = 'TIMEOUT'

    REFUNDED = 'REFUNDED'


class Result(Enum):
    SUCCESS = 'SUCCESS'
    UNFINISHED = 'UNFINISHED'  # can't tell yet
    CANCELED = 'CANCELED'  # user canceled
    FAILURE = 'FAILURE'

    STALE_CALLBACK = 'STALE_CALLBACK'

    @property
    def is_success(self):
        return self == Result.SUCCESS

    @property
    def is_error(self):
        return self == Result.FAILURE


class RefundResult(Enum):
    SUCCESS = 'SUCCESS'
    FAILURE = 'FAILURE'
