from decimal import Decimal
from typing import Dict, Optional

from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.db.transaction import atomic
from django.http import Http404, HttpResponse, HttpRequest
from django.utils.module_loading import import_string

from payments.const import Result, Status, RefundResult
from payments.exceptions import StatusError, InvalidAmountError
from payments.handler import BaseHandler
from payments.models import Transaction
from payments.providers.base import ProviderBase
from payments.dto import Purchase


class Manager:
    def __init__(self, providers: Dict[str, ProviderBase], handler: BaseHandler):
        self.providers = providers
        self.handler = handler

    @atomic
    def setup(self, purchase: Purchase, request: HttpRequest) -> str:
        provider = self.providers[purchase.provider]
        transaction = Transaction.objects.create(
            **provider.get_create_transaction_kwargs(purchase, request)
        )
        try:
            # @TODO callback to hander?
            return provider.setup(transaction, purchase, request)

        except Exception as e:  # @TODO more concrete exception
            transaction.frames.all().delete()
            transaction.delete()
            raise e

    @atomic
    def refund(self, transaction: Transaction, amount: Optional[Decimal] = None) -> RefundResult:
        if not transaction.is_refundable:
            raise StatusError("Transaction is not refundable")

        refundable_amount = transaction.refundable_amount

        if amount is None:
            amount = refundable_amount

        elif amount > refundable_amount:
            raise InvalidAmountError("Can't refund supplied amount")

        provider = self.providers[transaction.provider]
        result = provider.refund(transaction, amount)

        if result == RefundResult.SUCCESS:
            transaction.status = Status.REFUNDED
            transaction.refunded_amount += amount
            transaction.save()

        return result

    def check_pending_transactions(self, provider_id: Optional[str] = None):
        if provider_id:
            for result, transaction in self.providers[provider_id].check_pending_transactions():
                self.handler.handle_result(result, transaction)
        else:
            for provider in self.providers.values():
                for result, transaction in provider.check_pending_transactions():
                    self.handler.handle_result(result, transaction)

    def callback(self, request, provider_id, token=None, action=None) -> HttpResponse:
        try:
            provider = self.providers[provider_id]
        except KeyError:
            raise Http404

        # @TODO try:, except Transaction.DoesNotExist -- both branches
        if token:
            transaction = Transaction.objects.select_for_update().get(
                provider=provider_id,
                token=token
            )
        else:
            transaction = provider.get_transaction_from_request(request)

        if transaction.status == Status.CONFIRMED:
            """
            Users seem to be returning to already confirmed transactions 
            """
            self.handler.handle_result(Result.STALE_CALLBACK, transaction)
            return self.handler.handle_response(request, Result.STALE_CALLBACK, transaction)

        if action:
            method = f"process_{action}"
            callback_handler = getattr(provider, method, None)

            if callback_handler is None:
                raise Http404
        else:
            callback_handler = provider.process

        result = callback_handler(transaction, request)
        self.handler.handle_result(result, transaction)
        return self.handler.handle_response(request, result, transaction)


def _create_manager() -> Manager:
    try:
        provider_classes = settings.PAYMENT_PROVIDERS
    except AttributeError:
        raise ImproperlyConfigured("PAYMENT_PROVIDERS has to be defined")

    providers = {
        k.id: k() for k in [import_string(klass) for klass in provider_classes]
    }

    try:
        handler_class = import_string(settings.PAYMENT_HANDLER)
    except AttributeError:
        raise ImproperlyConfigured("PAYMENT_HANDLER has to be defined")

    return Manager(providers, handler_class())


manager = _create_manager()


