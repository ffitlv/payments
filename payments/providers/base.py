from datetime import timedelta
from decimal import Decimal
from typing import Optional, Iterable, Tuple, Dict
from urllib.parse import urljoin

from django.core.exceptions import ImproperlyConfigured
from django.conf import settings
from django.db.models import Q
from django.http import HttpRequest
from django.urls import reverse
from django.utils.timezone import now

from payments.const import Status, Result, RefundResult
from payments.models import Transaction
from payments.dto import Purchase
from payments.schema import PurchaseSchema

try:
    BASE_URL = settings.PAYMENT_BASE_PATH
except AttributeError:
    raise ImproperlyConfigured("PAYMENT_BASE_PATH has to be defined")


class ProviderBase:
    id = None

    def build_url(self, location: str) -> str:
        return urljoin(BASE_URL, location)

    def get_create_transaction_kwargs(self, purchase: Purchase, request: HttpRequest) -> Dict:
        purchase_data = PurchaseSchema().dump(purchase)
        return {
            'merchant_reference': purchase.merchant_reference,
            'purchase_data': purchase_data,
            'amount': purchase.amount,
            'provider': self.id,
        }

    def callback_url(self, token: str, action: Optional[str] = None) -> str:
        kwargs = {'provider_id': self.id, 'token': token}
        if action:
            kwargs['action'] = action

        return self.build_url(reverse('payments:callback', kwargs=kwargs))

    def get_pending_transactions(self, minutes_timeout=30) -> Iterable[Transaction]:
        t = now() - timedelta(minutes=minutes_timeout)
        return Transaction.objects.filter(
            Q(provider=self.id, status=Status.PENDING) &
            Q(
                Q(last_query_at__lt=t) |
                Q(last_query_at__isnull=True, created_at__lt=t)
            )
        )

    def check_pending_transactions(self) -> Iterable[Tuple[Result, Transaction]]:
        raise NotImplementedError

    def get_transaction_from_request(self, request: HttpRequest) -> Transaction:
        raise NotImplementedError

    def process(self, transaction: Transaction, request: HttpRequest) -> Result:
        raise NotImplementedError

    def setup(self, transaction: Transaction, purchase: Purchase, request: HttpRequest) -> str:
        """
        This setups payment and returns client redirect url
        If we ever need to setup without client it has to be different method anyway
        :param transaction:
        :param purchase:
        :param request:

        :return:
        """
        raise NotImplementedError

    def process_cancel(self, transaction: Transaction, request: HttpRequest) -> Result:
        transaction.status = Status.CANCELED
        transaction.save()
        return Result.CANCELED

    def refund(self, transaction: Transaction, amount: Decimal) -> RefundResult:
        # @TODO should be more concrete result type
        raise NotImplementedError
