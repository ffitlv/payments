from decimal import Decimal
from typing import Iterable, Tuple, Dict, Optional

import paypalrestsdk.core as paypal
import paypalrestsdk.v1.payments as payments
from braintreehttp import HttpError
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.db.transaction import atomic
from django.http import HttpRequest

from payments.const import Status, Result, RefundResult
from payments.exceptions import CommunicationError, ProviderError
from payments.models import Frame, Transaction
from payments.providers.base import ProviderBase
from payments.dto import Purchase


class PaypalException(ProviderError):
    def __init__(self, response: Dict, *args: object) -> None:
        self.response = response
        super().__init__(*args)


class AlreadyPaidException(PaypalException):
    pass


class InstrumentDeclinedException(PaypalException):
    pass


def _map_error(error: Dict):
    name = error.get('name', None)
    if name:
        if name == 'PAYMENT_ALREADY_DONE':
            raise AlreadyPaidException(error)
        elif name == 'INSTRUMENT_DECLINED':
            raise InstrumentDeclinedException(error)

    # We don't know what to do with this response, name missing
    raise PaypalException(error)


class Client(paypal.PayPalHttpClient):
    """
    Workaround for
    https://github.com/braintree/braintreehttp_python/issues/1
    https://github.com/braintree/braintreehttp_python/pull/3
    """

    def _create_frame(self, response) -> dict:
        status_code = response.status_code

        if 200 <= status_code <= 299:
            body = ""
            if response.text and (len(response.text) > 0 and response.text != 'None'):
                body = self.encoder.deserialize_response(response.text, response.headers)
            return body

        else:
            raise response.text

    def parse_response(self, response):
        parsed_response = super().parse_response(response)
        parsed_response.frame_data = self._create_frame(response)
        return parsed_response


def find_sale(payment_data: Dict) -> Optional[Dict]:
    for transaction in payment_data.get('transactions', []):
        for resource in transaction.get('related_resources', []):
            if 'sale' in resource:
                return resource['sale']


class ExpressProvider(ProviderBase):
    id = "paypal_express"

    def __init__(self):
        try:
            client_id = settings.PAYMENT_PAYPAL_EXPRESS_CLIENT
        except AttributeError:
            raise ImproperlyConfigured("PAYMENT_PAYPAL_EXPRESS_CLIENT required")

        try:
            client_secret = str(settings.PAYMENT_PAYPAL_EXPRESS_SECRET)
        except AttributeError:
            raise ImproperlyConfigured("PAYMENT_PAYPAL_EXPRESS_SECRET required")

        self.expirience_profile_id = getattr(settings, 'PAYMENT_PAYPAL_EXPIRIENCE_PROFILE_ID', None)

        use_sandbox = getattr(settings, 'PAYMENT_PAYPAL_EXPRESS_SANDBOX', settings.DEBUG)

        if use_sandbox:
            env = paypal.SandboxEnvironment(client_id, client_secret)
        else:
            env = paypal.LiveEnvironment(client_id, client_secret)

        self.client = Client(environment=env)

    def create_setup_request(self, purchase: Purchase, transaction: Transaction) -> dict:
        """
        We want this here so we can overload it for particular deployment
        """
        return {
            'intent': 'sale',
            'experience_profile_id': self.expirience_profile_id,
            'payer': {'payment_method': 'paypal'},
            'redirect_urls': {
                'return_url': self.callback_url(token=transaction.token),
                'cancel_url': self.callback_url(action='cancel', token=transaction.token)
            },
            'transactions': [
                {
                    'amount': {
                        'total': str(purchase.amount),
                        'currency': purchase.currency,
                        'details': {
                            'subtotal': str(purchase.subtotal),
                            'tax': str(purchase.tax),
                            'shipping': str(purchase.shipping)
                        } if purchase.subtotal else None
                    },
                    'item_list': {
                        'items': [{
                            'currency': purchase.currency,
                            'name': item.name,
                            'price': str(item.price),
                            'quantity': str(item.quantity)
                        } for item in purchase.items]
                    } if purchase.items else None,
                    'description': purchase.description
                }
            ]
        }

    def request(self, payload: any):
        try:
            return self.client.execute(payload)
        except HttpError as e:
            if e.status_code == 400:
                try:
                    _map_error(self.client.encoder.deserialize_response(e.message, e.headers))
                except IOError:
                    # Failed to deserialize body, re-raise original exception
                    raise CommunicationError(str(e))
            else:
                # What is the status code?
                raise CommunicationError(str(e))

        except IOError as ioe:
            raise CommunicationError(str(ioe))

    def setup(self, transaction: Transaction, purchase: Purchase, request: HttpRequest) -> str:
        payment_request = payments.PaymentCreateRequest()
        payment_request.request_body(self.create_setup_request(purchase, transaction))

        response = self.request(payment_request)
        payment = response.result

        transaction.provider_reference = payment.id
        transaction.status = Status.PENDING
        transaction.save()

        Frame.objects.create(transaction=transaction, request=payment_request.body, response=response.frame_data)

        for link in payment.links:
            if link.rel == 'approval_url':
                return str(link.href)
        else:
            raise ProviderError("Failed to find approval_url")

    def refund(self, transaction: Transaction, amount: Decimal) -> RefundResult:
        """
        This will refund "sale". It's possible to refund "captures" but we don't have em.
        :param transaction:
        :param amount:
        :return:
        """

        # DRY this?
        payment_request = payments.PaymentGetRequest(transaction.provider_reference)
        response = self.request(payment_request)
        Frame.objects.create(transaction=transaction, request=payment_request.body, response=response.frame_data)

        sale = find_sale(response.frame_data)

        if sale is None:
            raise ProviderError("Failed to find sale")

        # assert "state": "completed"? Whate if partially refunded?

        # just making sure it's refundable
        refund_link = None
        for link in sale.get("links", []):
            if link['rel'] == "refund":
                refund_link = link['href']

        if refund_link is None:
            raise ProviderError("Failed to find refund link")

        # assert sale amount?
        refund_request = payments.SaleRefundRequest(sale['id'])

        """
        @!IMPORTANT pending payments can cause errors: 
        INTERNAL_SERVICE_ERROR, or TRANSACTION_REFUSED
        """
        refund_request.request_body(
            # partial refund
            {
                "amount": {
                    "total": str(amount),
                    # assume same currency
                    "currency": sale['amount']['currency']
                },
                # consider passing theese down:
                # invoice_number
                # description
            } if amount != transaction.amount else {}
        )

        response = self.request(refund_request)
        Frame.objects.create(transaction=transaction, request=refund_request.body, response=response.frame_data)

        if response.frame_data.get('state', None) == 'completed':
            return RefundResult.SUCCESS

        # should we consider anything else?
        return RefundResult.FAILURE

    def get_transaction_info(self, transaction: Transaction):
        # @TODO set last_query_at?
        payment_request = payments.PaymentGetRequest(transaction.provider_reference)
        response = self.request(payment_request)
        Frame.objects.create(transaction=transaction, request=payment_request.body, response=response.frame_data)
        return response

    @atomic
    def finish_transaction(self, transaction: Transaction) -> Result:
        last_response = transaction.frames.last().response

        payer_info = last_response.get('payer', {}).get('payer_info', None)

        # What is this condition exactly?
        if payer_info is None and last_response.get('state') == 'created':
            # @TODO use braintree response?
            last_response = self.get_transaction_info(transaction).frame_data
            payer_info = last_response.get('payer', {}).get('payer_info', None)

            if payer_info is None and last_response.get('state') == 'created':
                transaction.status = Status.TIMEOUT
                transaction.save()

                return Result.CANCELED
            
            if last_response.get('state') == 'failed':
                transaction.status = Status.REJECTED
                transaction.save()
                return Result.FAILURE

        execute_request = payments.PaymentExecuteRequest(last_response.get('id'))\
            .request_body({"payer_id": payer_info.get('payer_id')})

        try:
            response = self.request(execute_request)
            Frame.objects.create(transaction=transaction, request=execute_request.body, response=response.frame_data)
            # should we look for sale here? Now we are proceeding if sale is pending
            if response.result.state == 'approved':
                result = Result.SUCCESS
                transaction.status = Status.CONFIRMED
            else:
                result = Result.FAILURE
                transaction.status = Status.REJECTED

        except AlreadyPaidException as e:
            Frame.objects.create(transaction=transaction, request=execute_request.body, response=e.response)
            transaction.status = Status.CONFIRMED
            result = Result.SUCCESS

        except InstrumentDeclinedException as e:
            Frame.objects.create(transaction=transaction, request=execute_request.body, response=e.response)
            result = Result.FAILURE
            transaction.status = Status.REJECTED

        transaction.save()
        return result

    @atomic
    def process(self, transaction: Transaction, request: HttpRequest) -> Result:
        return self.finish_transaction(transaction)

    def check_pending_transactions(self) -> Iterable[Tuple[Result, Transaction]]:
        for transaction in self.get_pending_transactions():
            yield self.finish_transaction(transaction), transaction


