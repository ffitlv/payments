import re
from typing import Dict

from payments.exceptions import ReponseParseError

TRANSACTION_ID = re.compile(r"TRANSACTION_ID: ([-A-Za-z0-9+/=]*)")


def parse_transaction_id(inp: str) -> str:
    match = TRANSACTION_ID.match(inp)
    if match:
        return match.group(1)

    raise ReponseParseError("Failed to parse transaction id")


KEYVAL = re.compile(r"^(?P<key>.*):\ (?P<value>.*)$", re.MULTILINE)


def parse_keyval(inp: str) -> Dict:
    res = {}
    for match in KEYVAL.finditer(inp):
        key, val = match.groups()
        res[key] = val

    return res
