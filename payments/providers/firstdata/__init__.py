from decimal import Decimal
from typing import Iterable, Tuple
from urllib.parse import urlencode

from django.http import HttpRequest
from django.utils.timezone import now

from payments.const import Status, Result, RefundResult
from payments.exceptions import ReponseParseError
from payments.models import Frame, Transaction
from payments.providers.base import ProviderBase
from payments.providers.firstdata.const import TransactionResult
from payments.providers.firstdata.merchant import Merchant
from payments.providers.firstdata.util import parse_keyval
from payments.dto import Purchase
from payments.utils import get_client_ip, get_config_key


class PayeezyProvider(ProviderBase):
    id = "firstdata_payeezy"

    def __init__(self):
        self.client_endpoint = get_config_key('PAYMENT_FIRSTDATA_PAYEEZY_CLIENT')
        self.merchant = Merchant(
            get_config_key('PAYMENT_FIRSTDATA_PAYEEZY_MERCHANT'),
            get_config_key('PAYMENT_FIRSTDATA_PAYEEZY_CERT')
        )

    def setup(self, transaction: Transaction, purchase: Purchase, request: HttpRequest) -> str:
        # @FIXME
        assert purchase.user_details is not None
        assert purchase.user_details.language_code is not None

        request_data, response_data = self.merchant.start_sms(
            purchase.amount,
            purchase.currency,
            get_client_ip(request),
            purchase.description,
            purchase.user_details.language_code
        )

        trans_id = response_data.get('TRANSACTION_ID', None)
        if not trans_id:
            raise ReponseParseError("Failed to parse transaction id")

        transaction.provider_reference = trans_id

        transaction.status = Status.PENDING
        transaction.save()

        Frame.objects.create(transaction=transaction, request=request_data, response=response_data)

        return f"{self.client_endpoint}?{urlencode({'trans_id': trans_id})}"

    def refund(self, transaction: Transaction, amount: Decimal) -> RefundResult:
        """
        1) DMS authorization (DMS1) could be reversed only during first 72 h
        when authorization registration (Step 1) was made. After 72 h First Data system
        will decline authorization reversal with response code 914.
        2) SMS and DMS transaction can be reversed independently if business day
        is closed or not.
        3) DMS and SMS reversal can be done 90 days from transaction date. After
        90 days system will reject reversals. Reversal can be sent just one time for each
        transaction.

        :param transaction:
        :param amount:
        :return:
        """
        request_data, response_data = self.merchant.reverse(transaction.provider_reference, amount)
        Frame.objects.create(transaction=transaction, request=request_data, response=response_data)
        transaction_result = TransactionResult.from_str(response_data['RESULT'])

        if transaction_result in [
            TransactionResult.OK,  # transaction reversed
            TransactionResult.REVERSED,  # already revered
        ]:
            return RefundResult.SUCCESS

        elif transaction_result == TransactionResult.FAILED:
            return RefundResult.FAILURE

        else:
            raise ValueError

    def get_transaction_from_request(self, request: HttpRequest) -> Transaction:
        """
        "When redirecting, additional parameters can be sent. Such parameters will be sent
        back to the merchant, when redirecting the client back to the merchant’s
        website, parameters can be received with POST method" - we could use this, but it's querystring/body anyway
        """
        return Transaction.objects.select_for_update().get(
            provider=self.id,
            provider_reference=request.POST.get('trans_id')
        )

    def query(self, transaction: Transaction, client_ip_addr: str) -> Result:
        request_data, response_data = self.merchant.get_result(transaction.provider_reference, client_ip_addr)
        Frame.objects.create(transaction=transaction, request=request_data, response=response_data)

        transaction_result = TransactionResult.from_str(response_data['RESULT'])
        transaction.result, transaction.status = transaction_result.map_gw_result()

        transaction.last_query_at = now()
        transaction.save()

        return transaction.result

    def process(self, transaction: Transaction, request: HttpRequest) -> Result:
        assert request.method == 'POST'

        # Why do we even care about this (there also seem to be two endpoints for no reason)? We have to query anyway
        _error = request.POST.get('error', None)
        return self.query(transaction, get_client_ip(request))

    def check_pending_transactions(self) -> Iterable[Tuple[Result, Transaction]]:
        """
        Note. Transaction result should NOT be requested unless a client returns to
        merchant’s returnOkUrl/returnFailUrl. In case if client does not return to
        returnOkUrl/returnFailUrl then result may be requested after 13 minutes.
        """
        for transaction in self.get_pending_transactions(minutes_timeout=13):
            # Let us get client ip from setup frame, not sure if it's necessary
            setup_request = transaction.frames.first().request
            client_ip_address = setup_request.get('client_ip_addr', None)
            yield self.query(transaction, client_ip_address), transaction

