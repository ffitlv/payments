from decimal import Decimal
from typing import Tuple, Dict

import requests

from payments.exceptions import CommunicationError
from .util import parse_keyval
from .const import CURRENCY_MAP, Command

Result = Tuple[Dict, Dict]


class Merchant:
    def __init__(self, endpoint: str, cert: Tuple[str, str]):
        self.endpoint = endpoint
        self.cert = cert

    def _request(self, payload: dict) -> Dict:
        # @FIXME? can't we peer-verify at least in production
        r = requests.post(self.endpoint, data=payload, cert=self.cert, verify=False)
        try:
            r.raise_for_status()
        except (requests.exceptions.ReadTimeout, requests.exceptions.ConnectionError) as e:
            raise CommunicationError(str(e))

        return parse_keyval(r.text)

    def start_sms(self,
                  amount: Decimal,
                  currency: str,
                  client_ip_addr: str,
                  description: str,
                  language: str) -> Result:
        request = {
            'command': Command.START_SMS.value,
            'amount': int(amount * 100),  # into cents
            'currency': CURRENCY_MAP[currency],
            'client_ip_addr': client_ip_addr,
            'description': description,
            'language': language
        }

        """
        TRANSACTION_ID: <trans_id>
        """
        return request, self._request(request)

    def get_result(self, trans_id: str, client_ip_addr: str) -> Result:
        request = {
            'command': Command.GET_RESULT.value,
            'trans_id': trans_id,
            'client_ip_addr': client_ip_addr
        }

        """
        RESULT: <result>
        RESULT_CODE: <result_code>
        3DSECURE: <3dsecure>
        AAV: <aav>
        RRN: <rrn>
        APPROVAL_CODE: <app_code>
        CARD_NUMBER: 555555*******4444
        """
        return request, self._request(request)

    def close_day(self) -> Result:
        request = {
            'command': Command.CLOSE_DAY.value
        }
        """
        RESULT: OK
        RESULT_CODE: 500
        FLD_074: 0
        FLD_075: 8
        FLD_076: 464
        FLD_077: 0
        FLD_086: 0
        FLD_087: 151100
        FLD_088: 24461939
        FLD_089: 0
        """
        return request, self._request(request)

    def start_dms(self, **kwargs):
        raise NotImplementedError

    def make_dms(self, **kwargs):
        raise NotImplementedError

    def reverse(self, trans_id: str, amount: Decimal) -> Result:
        request = {
            'command': Command.REVERSE.value,
            'trans_id': trans_id,
            'amount': int(amount * 100),  # into cents
        }
        """
        RESULT: OK
        RESULT_CODE: 400
        """
        return request, self._request(request)
