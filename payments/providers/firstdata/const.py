from enum import Enum
from typing import Tuple

from payments.const import Result, Status
from payments.exceptions import ReponseParseError

CURRENCY_MAP = {
    'EUR': 978,
    'USD': 840,
    'RSD': 941,
    'SKK': 703,
    'LTL': 440,
    'EEK': 233,
    'RUB': 643,
    'YUM': 891
}


class Command(Enum):
    START_SMS = 'v'
    START_DMS = 'a'
    MAKE_DMS = 't'
    GET_RESULT = 'c'
    REVERSE = 'r'
    CLOSE_DAY = 'b'


class TransactionResult(Enum):
    OK = 'OK'  # successful transaction
    FAILED = 'FAILED'  # failed transaction
    CREATED = 'CREATED'  # just registered in the system
    PENDING = 'PENDING'  # not yet performed
    DECLINED = 'DECLINED'  # its ECI value is included in the list of blocked ECI values (ECOMM server configuration)
    REVERSED = 'REVERSED'  # transaction already reversed
    AUTOREVERSED = 'AUTOREVERSED'  # transaction is automatically reversed if result was not requested within specified
    TIMEOUT = 'TIMEOUT'  # transaction declined due to timeout

    @classmethod
    def from_str(cls, value: str) -> 'TransactionResult':
        for m in TransactionResult:
            if value == m.value:
                return m

        raise ReponseParseError("Failed to parse transaction result")

    def map_gw_result(self) -> Tuple[Result, Status]:
        if self == TransactionResult.OK:
            return Result.SUCCESS, Status.CONFIRMED

        elif self in [
            TransactionResult.FAILED,
            TransactionResult.DECLINED,
        ]:
            return Result.FAILURE, Status.REJECTED

        elif self in [
            TransactionResult.AUTOREVERSED, TransactionResult.TIMEOUT
        ]:
            return Result.FAILURE, Status.TIMEOUT

        elif self in [
            TransactionResult.CREATED,
            TransactionResult.PENDING
        ]:
            return Result.UNFINISHED, Status.PENDING

        elif self == TransactionResult.REVERSED:
            raise NotImplementedError  # @TODO

        raise ValueError

