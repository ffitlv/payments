from _pydecimal import Decimal
from datetime import timedelta
from typing import Tuple, Dict, Iterable

import requests
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.db.transaction import atomic
from django.http import HttpRequest
from django.utils.timezone import now

from payments.const import Status, Result, RefundResult
from payments.exceptions import CommunicationError
from payments.models import Frame, Transaction
from payments.providers.base import ProviderBase
from payments.providers.swedbank.const import ReturnCode
from payments.providers.swedbank.exceptions import StatusError

from payments.dto import Purchase, Address
from payments.utils import get_client_ip

from .dto import response as resp, request as req, setup


"""
Hosted pagesets are "templates"
Hosted Page Set IDs:
"""
HOSTED_PAGE_SETS = {
    "PROD": {"en": 2207, "et": 2209, "lv": 2211, "lt": 2213, "ru": 2215},
    "DEV": {"en": 164, "et": 166, "lv": 168, "lt": 170, "ru": 172},
}

ENDPOINTS = {
    "PROD": (
        "https://mars.transaction.datacash.com/Transaction",
        "https://venus.transaction.datacash.com/Transaction",
    ),
    "DEV": ("https://accreditation.datacash.com/Transaction/acq_a",),
}


def _addrfields(address: Address) -> Dict:
    return {
        "address_line1": address.line_1,
        "address_line2": address.line_2,
        "city": address.city,
        "country": address.country_code,
        "zip_code": address.postal_code,
    }


class HPSProvider(ProviderBase):
    id = "swedbank_hps"

    def __init__(self):
        try:
            client = str(settings.PAYMENT_SWEDBANK_HPS_CLIENT)
        except AttributeError:
            raise ImproperlyConfigured("PAYMENT_SWEDBANK_HPS_CLIENT required")

        try:
            password = settings.PAYMENT_SWEDBANK_HPS_PASSWORD
        except AttributeError:
            raise ImproperlyConfigured("PAYMENT_SWEDBANK_HPS_PASSWORD required")

        self.authentication = req.Authentication(client=client, password=password)

        try:
            self.env = settings.PAYMENT_SWEDBANK_ENV
        except AttributeError:
            self.env = "DEV" if settings.DEBUG else "PROD"

    def create_setup_request(
        self, purchase: Purchase, transaction: Transaction, client_ip: str
    ):
        assert purchase.shipping_address is not None
        assert purchase.billing_address is not None

        # This has to be unique per transaction, we could use token but this is what reference impl had.
        merchant_reference = f"{transaction.merchant_reference}/{int(transaction.created_at.timestamp() % 999)}".rjust(
            16, "0"
        )

        user = purchase.user_details
        billing = purchase.billing_address
        shipping = purchase.shipping_address

        billing_details = setup.risk.BillingDetails(
            name=f"{(billing.first_name or '')} {(billing.last_name or '')}".strip(),
            state_province=billing.state,
            **_addrfields(billing),
        )

        personal_details = setup.risk.PersonalDetails(
            first_name=user.first_name,
            surname=user.last_name,
            telephone=user.phone,
        )

        shipping_details = setup.risk.ShippingDetails(
            title=shipping.title,
            first_name=shipping.first_name,
            surname=shipping.last_name,
            **_addrfields(shipping),
        )

        return req.SetupRequest(
            self.authentication,
            setup.SetupTransaction(
                details=setup.Details(
                    merchant_reference=merchant_reference,
                    amount=purchase.amount,
                    currency=purchase.currency,
                    purchase_datetime=transaction.created_at,
                    merchant_url=settings.PAYMENT_BASE_PATH,
                    purchase_desc=purchase.description,
                    risk=setup.Risk(
                        customer_details=setup.risk.CustomerDetails(
                            billing_details=billing_details,
                            personal_details=personal_details,
                            shipping_details=shipping_details,
                            ip_address=client_ip,
                            email_address=user.email,
                        ),
                    ),
                ),
                hosted_page_details=setup.HostedPageDetails(
                    page_set_id=HOSTED_PAGE_SETS[self.env][
                        purchase.user_details.language_code
                    ],
                    return_url=self.callback_url(transaction.token),
                    expiry_url=self.callback_url(transaction.token),
                    error_url=self.callback_url(transaction.token),
                    go_back_url=self.callback_url(transaction.token, "cancel"),
                ),
            ),
        )

    def fetch(self, r: req.RequestBase) -> requests.Response:
        last_exception = None
        for endpoint in ENDPOINTS[self.env]:
            try:
                resp = requests.get(endpoint, data=r.dump_xml(), timeout=10)
                resp.raise_for_status()
                return resp

            # for what other exceptions should we proceeed?
            except (
                requests.exceptions.ReadTimeout,
                requests.exceptions.ConnectionError,
            ) as e:
                last_exception = e

        raise CommunicationError(str(last_exception))

    def setup(
        self, transaction: Transaction, purchase: Purchase, request: HttpRequest
    ) -> str:
        setup_request = self.create_setup_request(
            purchase, transaction, get_client_ip(request)
        )

        setup_response = resp.SetupResponse.load_xml(self.fetch(setup_request).content)

        # Bail if we failed to setup. Perhaps we should keep frames and transaction?
        setup_response.status.raise_for_status()

        transaction.provider_reference = setup_response.datacash_reference
        transaction.status = Status.PENDING
        transaction.save()

        Frame.objects.create(
            transaction=transaction,
            request=setup_request.dump(),
            response=setup_response.dump(),
        )

        return str(setup_response.host_page_details.redirect_url)

    def _query(
        self, transaction: Transaction
    ) -> Tuple[req.QueryRequest, resp.QueryResponse]:
        query_request = req.QueryRequest(
            self.authentication, transaction.provider_reference
        )
        query_response = resp.QueryResponse.load_xml(self.fetch(query_request).content)

        Frame.objects.create(
            transaction=transaction,
            request=query_request.dump(),
            response=query_response.dump(),
        )
        transaction.last_query_at = now()
        transaction.save()

        return query_request, query_response

    def query(self, transaction: Transaction):
        query_request, query_response = self._query(transaction)

        result = query_response.result

        if result == Result.UNFINISHED:
            # transaction is still in flight
            pass

        elif result == Result.SUCCESS:
            # we have valid auth_reference let us authorize payment
            auth_request = req.AuthRequest(
                self.authentication, query_response.auth_reference
            )
            auth_response = resp.AuthResponse.load_xml(self.fetch(auth_request).content)

            Frame.objects.create(
                transaction=transaction,
                request=auth_request.dump(),
                response=auth_response.dump(),
            )

            # Should we look at auth response? We used to not even persist it

            transaction.status = Status.CONFIRMED

        elif result == Result.FAILURE:
            transaction.status = Status.REJECTED

        transaction.save()

        return result

    def refund(self, transaction: Transaction, amount: Decimal) -> RefundResult:
        """
        A merchant may only refund a previously authorized purchase transaction.
        A refund is performed by submitting a simple request to the Payment Gateway
        with the reference of the transaction to be refunded. A total amount of refund
        requests should not exceed the original value of authorization request.
        The Payment Gateway will retrieve the relevant card details from the original
        transaction and submit the refund for processing.
        """

        # Should we look for
        query_request, query_response = self._query(transaction)

        assert query_response.result == Result.SUCCESS

        refund_request = req.RefundRequest(
            self.authentication, query_response.auth_reference, amount
        )
        refund_response = resp.RefundResponse.load_xml(
            self.fetch(refund_request).content
        )

        Frame.objects.create(
            transaction=transaction,
            request=refund_request.dump(),
            response=refund_response.dump(),
        )

        if refund_response.status == ReturnCode.SUCCESS:
            return RefundResult.SUCCESS

        # should we consider anything else?
        return RefundResult.FAILURE

    @atomic
    def process(self, transaction: Transaction, request: HttpRequest) -> Result:
        # payment_id = request.GET.get('dts_reference')

        result = self.query(transaction)
        if result == Result.UNFINISHED:
            """
            User is back but failed to finish transaction in time
            Happens if we are still waiting for ACS for instance
            """
            transaction.status = Status.REJECTED
            transaction.save()
            result = Result.FAILURE
        return result

    def transaction_is_expired(self, transaction: Transaction) -> bool:
        return transaction.created_at < now() - timedelta(hours=3)  # @TODO const

    def check_pending_transactions(self) -> Iterable[Tuple[Result, Transaction]]:
        for transaction in self.get_pending_transactions():
            if self.transaction_is_expired(transaction):
                transaction.status = Status.TIMEOUT
                transaction.save()
                yield Result.FAILURE, transaction
            else:
                yield self.query(transaction), transaction
