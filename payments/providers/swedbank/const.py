from enum import IntEnum

from payments.providers.swedbank.exceptions import StatusError


class ReturnCode(IntEnum):
    """"Enumerates HPS response codes"""

    """Transaction accepted and logged."""
    SUCCESS = 1

    """
    Socket write error Communication was interrupted.
    The argument is e.g. 523/555 (523 bytes written but 555 expected)."""
    SOCKET_WRITE_ERROR = 2

    """A timeout occurred while we were reading the transaction details."""
    TIMEOUT = 3

    """
    A field was specified twice, you sent us too much or invalid data, a pre-auth lookup failed during a fulfil
    transaction, the swipe field was incorrectly specified, or you omitted a field. The argument will give a better
    indication of what exactly went wrong."""
    EDIT_ERROR = 5

    """Error in communications link; resend"""
    COMMS_ERROR = 6

    """
    Transaction declined. The arguments are as return code 1, except the first argument is the bank's reason for
    declining it (e.g. REFERRAL, CALL AUTH CENTRE, PICK UP CARD etc.) or the result from a failed fraud check
    (e.g. FRAUD DECLINED reason code)"""
    NOT_AUTHORISED = 7

    """The currency you specified does not exist"""
    CURRENCY_ERROR = 9

    """The vTID or password were incorrect"""
    AUTHENTICATION_ERROR = 10

    """
    You attempted to fulfill a transaction that either could not be fulfilled (e.g. auth, refund)
    or already has been."""
    CANNOT_FULFILL_TRANSACTION = 19

    """A successful transaction has already been sent using this vTID and reference number."""
    DUPLICATE_TRANSACTION_REFERENCE = 20

    """This terminal does not accept transactions for this type of card (e.g. Diner's Club, American Express if the
    merchant does not take American Express, Domestic Maestro if multicurrency only)."""
    INVALID_CARD_TYPE = 21

    """Reference numbers should be 16 digits for fulfill transactions, or between 6 and 30 digits for all others."""
    INVALID_REFERENCE = 22

    """The expiry dates should be specified as MM/YY or MM-YY"""
    EXPIRY_DATE_INVALID = 23

    """The supplied expiry date is in the past."""
    CARD_HAS_ALREADY_EXPIRED = 24

    """Card number invalid The card number does not pass the standard Luhn checksum test."""
    CARD_NUMBER_INVALID = 25

    MALFORMED_XML = 60

    """Information was found in the HPS requests that is not applicable to the request type"""
    HPS_INAPPROPRIATE_DATE_SUPPLIED = 810

    """The submitted HPS request was missing mandatory data"""
    HPS_MISSING_DATA = 811

    """The supplied DataCash reference is not valid"""
    HPS_INVALID_REFERNECE = 812

    """A problem was encountered while sending a request to or receiving a response from the HPS"""
    HPS_COMMUNICATIONS_ERROR = 813

    """The HPS did not return a valid response"""
    HPS_INVALID_HPS_RESPONSE = 814

    """The supplied DataCash reference cannot be used to perform a payment"""
    HPS_INVALID_PAYMENT_REFERNCE = 815

    """The HPS did not find configuration for your merchant account."""
    HPS_MERCHANT_NOT_CONFIGURED = 816

    """The dynamic capture field data specified is too long."""
    HPS_CAPTURE_FIELD_DATA_TOO_LONG = 817

    """The Dynamic Data Field value is too long."""
    HPS_DYNAMICE_FIELD_DATA_TOO_LONG = 818

    HPS_AWAITING_CUSTOMER_CARD_DETAILS = 820
    HPS_ATLEAST_ONE_AUTH_ATTEMPTED = 821
    HPS_MAXIMUM_NUMBER_OF_RETRY = 822
    HPS_MERCHANTS_CUSTOMER_SESSION_TIMEOUT = 823
    HPS_PAYMENT_IN_FLIGHT = 824
    HPS_MERCHANT_NOT_CONFIGURED_FULL_HPS = 825
    HPS_TRANSACTION_ALREADY_MARKED_FINAL = 826
    HPS_INVALID_CAPUTRE_METHOD = 827
    HPS_AWAITING_AUTH = 828
    HPS_INVALID_PAYMENT_METHOD_FULL_HPS = 829

    HPS_PAYPAL_CUSTOMER_LOGIN = 845
    HPS_PAYPAL_WAITING_CONFIRM = 846
    HPS_PAYPAL_PAYMENT_CONFIRMED_FUNDS = 874

    """The supplied token does not comply with the token format specification"""
    T_INVALID_TOKEN = 991

    """Unknown token supplied"""
    T_UNKNOWN_TOKEN = 992

    """The merchant is not configured for the Tokenization service"""
    T_NOT_CONFIGURED_FOR_SERVICE = 993

    """The supplied Token has expired"""
    T_EXPIRED_TOKEN = 994

    """Token generation is already taking place for this PAN The card number supplied is currently being tokenised by
    another transaction, please try again"""
    T_TOKEN_ALREADY_GENERATING = 995

    """The supplied shared secret does not comply with the shared secret format specification"""
    T_INVALID_SHARED_SECRET = 996

    """Token generation resulted in a collision"""
    T_COLLISION = 997

    @property
    def is_success(self) -> bool:
        return self == ReturnCode.SUCCESS

    def as_error(self) -> StatusError:
        return StatusError(str(self))

    def raise_for_status(self):
        if not self.is_success:
            raise self.as_error()

    @property
    def is_final(self) -> bool:
        # not temp
        return self not in [
            ReturnCode.HPS_AWAITING_CUSTOMER_CARD_DETAILS,
            ReturnCode.HPS_PAYMENT_IN_FLIGHT,
            # ReturnCode.HPS_AWAITING_AUTH,
            ReturnCode.HPS_PAYPAL_CUSTOMER_LOGIN,
            ReturnCode.HPS_PAYPAL_WAITING_CONFIRM,
            ReturnCode.HPS_PAYPAL_PAYMENT_CONFIRMED_FUNDS
        ]
