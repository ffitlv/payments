from datetime import date
from enum import Enum
from typing import Optional, Dict

from dataclasses import dataclass
from lxml.builder import E

from payments.utils import STATE_PROVINCE_MAP


def xstr(s: Optional[str]) -> str:
    return '' if s is None else str(s)


@dataclass
class BillingDetails:
    name: Optional[str] = None
    address_line1: Optional[str] = None
    address_line2: Optional[str] = None
    city: Optional[str] = None
    country: Optional[str] = None
    zip_code: Optional[str] = None
    state_province: Optional[str] = None

    def as_e(self):
        fields = []
        if self.state_province:
            try:
                verbose_province = STATE_PROVINCE_MAP[self.country][self.state_province]
            except KeyError:
                verbose_province = self.state_province

            fields = [E.state_province(verbose_province)]

        fields += [
            E.name(xstr(self.name)),
            E.address_line1(xstr(self.address_line1)),
            E.address_line2(xstr(self.address_line2)),
            E.city(xstr(self.city)),
            E.zip_code(xstr(self.zip_code)),
            E.country(xstr(self.country))
        ]

        return E.BillingDetails(*fields)


@dataclass
class PersonalDetails:
    first_name: Optional[str] = None
    surname: Optional[str] = None
    telephone: Optional[str] = None
    date_of_birth: Optional[date] = None

    def as_e(self):
        personal_details = []
        if self.date_of_birth:
            personal_details.append(
                E.date_of_birth(self.date_of_birth.strftime("%Y-%m-%d"))
            )

        personal_details += [
            E.first_name(xstr(self.first_name)),
            E.surname(xstr(self.surname)),
            E.telephone(xstr(self.telephone))
        ]

        return E.PersonalDetails(*personal_details)


@dataclass
class ShippingDetails:
    title: Optional[str] = None
    first_name: Optional[str] = None
    surname: Optional[str] = None
    address_line1: Optional[str] = None
    address_line2: Optional[str] = None
    city: Optional[str] = None
    country: Optional[str] = None
    zip_code: Optional[str] = None

    def as_e(self):
        return E.ShippingDetails(
            E.title(xstr(self.title)),
            E.first_name(xstr(self.first_name)),
            E.surname(xstr(self.surname)),
            E.address_line1(xstr(self.address_line1)),
            E.address_line2(xstr(self.address_line2)),
            E.city(xstr(self.city)),
            E.country(xstr(self.country)),
            E.zip_code(xstr(self.zip_code))
        )


@dataclass
class CustomerDetails:
    billing_details: BillingDetails
    personal_details: PersonalDetails
    shipping_details: ShippingDetails

    # RiskDetails
    ip_address: str
    email_address: Optional[str] = None

    def as_e(self):
        risk_details = [E.email_address(self.email_address)] if self.email_address else []
        risk_details.append(E.ip_address(self.ip_address))

        return E.CustomerDetails(
            E.OrderDetails(
                self.billing_details.as_e(),
            ),
            self.personal_details.as_e(),
            self.shipping_details.as_e(),
            # This is the mechanism with which the client chooses to purchase.
            # Should be populated with CC = Bank Card
            E.PaymentDetails(E.payment_method("CC")),
            E.RiskDetails(*risk_details)
        )


class Action(Enum):
    # ? strings according to spec
    PRE_AUTH = "1"
    POST_AUTH = "2"


class Channel(Enum):
    PHYSICAL = 'P'
    MOTO = 'M'
    WEB = 'W'
    KIOSK = 'K'
    IN_STORE = 'I'
    MAIL_ORDER = 'S'
    OTHER = 'O'


@dataclass
class Risk:
    customer_details: CustomerDetails
    channel: Optional[Channel] = Channel.WEB
    merchant_location: Optional[str] = None
    action: Optional[Action] = Action.PRE_AUTH

    def as_e(self):
        configuration = [E.channel(self.channel.value)]

        if self.merchant_location:
            configuration.append(E.merchant_location(self.merchant_location))

        return E.Risk(
            E.Action(
                E.MerchantConfiguration(*configuration),
                self.customer_details.as_e(),
                service=self.action.value
            )
        )
