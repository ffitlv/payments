from datetime import datetime
from decimal import Decimal
from typing import Optional, Dict

from dataclasses import dataclass
from lxml.builder import E
from .risk import Risk


@dataclass
class Details:
    merchant_reference: str
    amount: Decimal
    currency: str

    # The container for 3-D Secure details
    purchase_datetime: datetime
    merchant_url: str
    purchase_desc: str
    risk: Risk

    def as_e(self):
        return E.TxnDetails(
            E.merchantreference(self.merchant_reference),
            # The amount to be authorised and an indication of the currency to be used. EUR only.
            E.amount(str(self.amount), currency=self.currency),
            # Indicates the environment from which the transaction has been processed.
            # "ecomm" – e.g. use for website / Internet / mobile
            E.capturemethod("ecomm"),
            E.ThreeDSecure(
                E.purchase_datetime(self.purchase_datetime.strftime("%Y%m%d %H:%M:%S")),
                # Instructs the Payment Gateway to attempt 3-D Secure authentication process.
                # 3-D Secure is mandated and must always be attempted.
                # "yes" – attempt 3-D Secure
                E.verify("yes"),
                E.merchant_url(self.merchant_url),
                E.purchase_desc(self.purchase_desc)
            ),
            self.risk.as_e()
        )


@dataclass
class HostedPageDetails:
    page_set_id: int
    return_url: str
    expiry_url: str
    error_url: str

    show_card_holder_name: Optional[bool] = False
    go_back_url: Optional[str] = None

    def as_e(self):
        args = [
            # Indicates the Hosted Method to be used. Value must match = "setup_full"
            E.method("setup_full"),
            E.page_set_id(str(self.page_set_id)),
            E.return_url(self.return_url),
            E.expiry_url(self.expiry_url),
            E.error_url(self.error_url),
        ]

        dyn_data = []
        if self.show_card_holder_name:
            dyn_data.append(E.dyn_data_3("show"))

        if self.go_back_url:
            dyn_data.append(E.dyn_data_4(self.go_back_url))

        if dyn_data:
            args.append(E.DynamicData(*dyn_data))

        return E.HpsTxn(*args)


@dataclass
class SetupTransaction:
    details: Details
    hosted_page_details: HostedPageDetails

    def as_e(self):
        return E.Transaction(
            self.details.as_e(),
            self.hosted_page_details.as_e(),
            E.CardTxn(
                # indicates transaction is to be authorized in one stage processing model and will be marked for
                # settlement automatically by the gateway.
                E.method("auth")
            )
        )
