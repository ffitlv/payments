from decimal import Decimal
from enum import Enum
from typing import Union, Dict

from lxml import etree
from lxml.builder import E
from dataclasses import dataclass, asdict

from payments.dto import DTO
from payments.models import Transaction
from payments.providers.swedbank.dto.setup import SetupTransaction


VERSION = "2"


@dataclass
class Authentication:
    password: str
    client: str

    def as_e(self):
        return E.Authentication(
            E.password(self.password),
            E.client(self.client)
        )

    def dump(self) -> Dict:
        # obfuscate
        return {
            'password': '********',
            'client': '********'
        }


class Method(Enum):
    QUERY = 'query'
    CANCEL = 'cancel'
    REFUND = 'txn_refund'


@dataclass
class Transaction:
    reference: str
    method: Method

    def as_e(self):
        return E.Transaction(
            E.HistoricTxn(
                E.reference(self.reference),
                E.method(self.method.value)
            )
        )


@dataclass
class RefundTransaction(Transaction):
    amount: Decimal

    # currency?

    def as_e(self):
        return E.Transaction(
            E.HistoricTxn(
                E.reference(self.reference),
                E.method(self.method.value)
            ),
            E.TxnDetails(
                E.amount(str(self.amount))
            )
        )


@dataclass
class RequestBase(DTO):
    authentication: Authentication
    transaction: Union[SetupTransaction, Transaction]

    def as_e(self):
        return E.Request(
            self.authentication.as_e(),
            self.transaction.as_e(),
            version=VERSION
        )

    def dump_xml(self, xml_declaration=True, pretty_print=True, encoding='UTF-8'):
        return etree.tostring(
            self.as_e(),
            xml_declaration=xml_declaration,
            pretty_print=pretty_print,
            encoding=encoding,
        )


@dataclass
class QueryRequest(RequestBase):
    def __init__(self, authentication: Authentication, reference: str):
        super().__init__(authentication=authentication,
                         transaction=Transaction(reference=reference, method=Method.QUERY))


class AuthRequest(QueryRequest):
    pass


@dataclass
class CancelRequest(RequestBase):
    def __init__(self, authentication: Authentication, reference: str):
        super().__init__(authentication=authentication,
                         transaction=Transaction(reference=reference, method=Method.CANCEL))


@dataclass
class RefundRequest(RequestBase):
    def __init__(self, authentication: Authentication, reference: str, amount: Decimal):
        super().__init__(authentication=authentication,
                         transaction=RefundTransaction(amount=amount, reference=reference, method=Method.REFUND))


class SetupRequest(RequestBase):
    def __init__(self, authentication: Authentication, transaction: SetupTransaction):
        super().__init__(
            authentication=authentication,
            transaction=transaction
        )
