from datetime import datetime
from typing import Dict, List, Optional
from urllib.parse import urlencode

from dataclasses import dataclass
from lxml import etree

from payments.dto import DTO
from payments.const import Result
from ..const import ReturnCode


def strings(xml, *fields) -> Dict:  # @TODO move up?
    return {field: xml.findtext(field) for field in fields}


def ints(xml, *fields) -> Dict:  # @TODO move up?
    return {field: int(xml.findtext(field)) for field in fields}


DATE_INP_FORMAT = '%Y-%m-%d %H:%M:%S'  # YYYY-MM-DD hh:mm:ss, 2015-06-18 11:17:26


@dataclass
class ResponseBase(DTO):
    mode: str
    reason: str
    status: ReturnCode
    time: int

    @classmethod
    def load_xml(cls, xml_str: str):
        xml = etree.fromstring(xml_str)
        assert xml.tag == 'Response'
        return cls.load(cls.parse_xml_fields(xml))

    @classmethod
    def load(cls, data):
        data['status'] = ReturnCode(data['status'])
        return cls(**data)

    @classmethod
    def parse_xml_fields(cls, xml) -> Dict:
        return {
            **strings(xml, 'mode', 'reason'),
            **ints(xml, 'status', 'time'),
        }


@dataclass
class HostedPageDetails:
    hps_url: str
    session_id: str

    @property
    def redirect_url(self):
        return f"{self.hps_url}?{urlencode({'HPS_SessionID': self.session_id})}"


@dataclass
class SetupResponse(ResponseBase):
    datacash_reference: str
    merchantreference: str
    host_page_details: Optional[HostedPageDetails]

    @classmethod
    def parse_xml_fields(cls, xml) -> Dict:
        xml_fields = super().parse_xml_fields(xml)

        hps_txn = xml.find("HpsTxn")

        xml_fields.update({
            "host_page_details": {
                "hps_url": hps_txn.findtext('hps_url'),
                "session_id": hps_txn.findtext('session_id')
            } if hps_txn is not None else None,
            **strings(xml, 'datacash_reference', 'merchantreference')
        })
        return xml_fields

    @classmethod
    def load(cls, data):
        if data['host_page_details'] is not None:
            data['host_page_details'] = HostedPageDetails(**data['host_page_details'])

        return super().load(data)


@dataclass
class AuthAttempt:
    dc_response: int
    reason: str
    datacash_reference: str = None


@dataclass
class QueryResponse(ResponseBase):
    datacash_reference: str
    information: str
    merchantreference: str
    auth_attempts: List[AuthAttempt] = None

    @classmethod
    def parse_xml_fields(cls, xml) -> Dict:
        xml_fields = super().parse_xml_fields(xml)

        auth_attempts = None

        attempts = xml.findall(".//AuthAttempts/Attempt")
        if attempts:
            auth_attempts = [{
                'dc_response': int(a.findtext('dc_response')),
                **strings(a, 'datacash_reference', 'reason')
            } for a in attempts]

        xml_fields.update({
            "auth_attempts": auth_attempts,
            **strings(xml, 'datacash_reference', 'information', 'merchantreference')
        })
        return xml_fields

    @classmethod
    def load(cls, data):
        if data['auth_attempts'] is not None:
            data['auth_attempts'] = [AuthAttempt(**attempt) for attempt in data['auth_attempts']]

        return super().load(data)

    @property
    def auth_reference(self) -> Optional[str]:
        if self.auth_attempts is not None:
            for attempt in self.auth_attempts:
                if attempt.dc_response == ReturnCode.SUCCESS:
                    return attempt.datacash_reference
        raise ValueError

    @property
    def result(self) -> Result:
        if self.status == ReturnCode.HPS_MERCHANTS_CUSTOMER_SESSION_TIMEOUT:
            return Result.FAILURE

        if not self.auth_attempts:
            return Result.UNFINISHED

        if not ReturnCode(self.status).is_final:
            return Result.UNFINISHED

        for attempt in self.auth_attempts:
            if attempt.dc_response == ReturnCode.SUCCESS:
                return Result.SUCCESS

        return Result.FAILURE


@dataclass
class Card:
    cv2avs_status: str
    card_category: str  # ENUM?
    expirydate: str
    country: str  # optional
    issuer: str
    pan: str
    scheme: str
    token: str


@dataclass
class TransactionResult:
    card: Card
    acquirer: str
    authcode: str
    datacash_reference: str
    environment: str
    merchant_reference: str
    reason: str
    sent: str
    status: str

    fulfill_timestamp: int
    transaction_timestamp: int

    fulfill_date: datetime
    transaction_date: datetime

    @classmethod
    def parse_xml_fields(cls, xml) -> Dict:
        return {
            'card': {
                'cv2avs_status': xml.findtext('Card/Cv2Avs/cv2avs_status'),
                **strings(
                    xml.find('Card'),
                    'card_category',
                    'expirydate',
                    'country',
                    'issuer',
                    'pan',
                    'scheme',
                    'token',
                )
            },
            'fulfill_date': datetime.strptime(xml.findtext('fulfill_date'), DATE_INP_FORMAT),
            'transaction_date': datetime.strptime(xml.findtext('transaction_date'), DATE_INP_FORMAT),
            **strings(
                xml,
                'acquirer',
                'authcode',
                'datacash_reference',
                'environment',
                'merchant_reference',
                'reason',
                'sent',
                'status'
            ),
            **ints(
                xml,
                'fulfill_timestamp',
                'transaction_timestamp'
            )
        }

    @classmethod
    def load(cls, data):
        data['card'] = Card(**data['card'])
        return cls(**data)


@dataclass
class RiskResponse:
    # additional_messages ?
    # cpi_value
    response_code: str
    response_message: str
    transaction_id: str


@dataclass
class Risk:
    screening_response: RiskResponse

    # bankresult_response

    @classmethod
    def parse_xml_fields(cls, xml) -> Dict:
        screening = xml.find('action_response/screening_response')
        return {
            'screening_response': strings(screening, 'response_code', 'response_message', 'transaction_id')
            if screening is not None else None
        }

    @classmethod
    def load(cls, data):
        if data['screening_response'] is not None:
            data['screening_response'] = RiskResponse(**data['screening_response'])

        return cls(**data)


@dataclass
class AuthResponse(ResponseBase):
    transaction_result: TransactionResult
    risk: Risk

    @classmethod
    def parse_xml_fields(cls, xml) -> Dict:
        xml_fields = super().parse_xml_fields(xml)
        xml_fields.update({
            "transaction_result": TransactionResult.parse_xml_fields(xml.find("QueryTxnResult")),
            "risk": Risk.parse_xml_fields(xml.find("Risk"))
        })
        return xml_fields

    @classmethod
    def load(cls, data):
        data['risk'] = Risk.load(data['risk'])
        data['transaction_result'] = TransactionResult.load(data['transaction_result'])
        return super().load(data)


@dataclass
class RefundResponse(ResponseBase):
    authcode: str
    acquirer: str
    datacash_reference: str
    merchantreference: str
    mid: str

    @classmethod
    def parse_xml_fields(cls, xml) -> Dict:
        return {
            'authcode': xml.findtext("HistoricTxn/authcode"),
            **super().parse_xml_fields(xml),
            **strings(
                xml,
                'acquirer',
                'datacash_reference',
                'merchantreference',
                'mid'
            )
        }
