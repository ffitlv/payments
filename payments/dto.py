import datetime
from enum import Enum
from typing import Optional, List, Dict, Any
from decimal import Decimal
from dataclasses import dataclass, fields, is_dataclass


def _dump_value(obj: Any, recurse: bool = True) -> Any:
    if is_dataclass(obj):
        if recurse and hasattr(obj, 'dump'):
            return obj.dump()

        return {
            f.name: _dump_value(getattr(obj, f.name))
            for f in fields(obj)
        }

    if isinstance(obj, list):
        return [_dump_value(v) for v in obj]

    if isinstance(obj, Decimal):
        return str(obj)

    if isinstance(obj, (datetime.datetime, datetime.date)):
        return obj.isoformat()

    if isinstance(obj, Enum):
        return obj.value

    return obj


class DTO:
    def dump(self) -> Dict:
        return _dump_value(self, False)


@dataclass
class Item:
    name: str
    quantity: int
    price: Decimal
    description: Optional[str] = None


@dataclass
class Address:
    country_code: str  # ISO3166-1
    line_1: str
    line_2: Optional[str] = None

    title: Optional[str] = None  # english speaking prefix
    first_name: Optional[str] = None
    last_name: Optional[str] = None

    city: Optional[str] = None
    postal_code: Optional[str] = None
    state: Optional[str] = None
    phone: Optional[str] = None


@dataclass
class UserDetails:
    language_code: str  # ISO 639-1 ("en", "ru", "lv", etc.)
    first_name: Optional[str] = None
    last_name: Optional[str] = None
    phone: Optional[str] = None
    email: Optional[str] = None


@dataclass
class Purchase:
    merchant_reference: str  # Merchant order or invoice id
    provider: str  # Provider id
    amount: Decimal  # Purchase amount
    currency: str  # ISO 4217 ("USD", "EUR", etc.)
    description: str  # Purchase description
    user_details: UserDetails

    items: Optional[List[Item]] = None
    subtotal: Optional[Decimal] = None  # Sum of items
    tax: Optional[Decimal] = None
    shipping: Optional[Decimal] = None

    billing_address: Optional[Address] = None
    shipping_address: Optional[Address] = None
