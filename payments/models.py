from decimal import Decimal

from django.contrib.postgres.fields import JSONField
from django.core.serializers.json import DjangoJSONEncoder
from django.db import models
from django.utils.crypto import get_random_string
from django.utils.timezone import now
from enumfields import EnumField

from payments.const import Status


class Transaction(models.Model):
    token = models.CharField(max_length=30, primary_key=True)
    merchant_reference = models.CharField(max_length=50, db_index=True)
    provider_reference = models.CharField(max_length=255, null=True, db_index=True)
    provider = models.CharField(max_length=50)
    status = EnumField(Status, max_length=20, default=Status.CREATED)

    created_at = models.DateTimeField(default=now)
    edited_at = models.DateTimeField(auto_now=True)
    last_query_at = models.DateTimeField(null=True)

    amount = models.DecimalField(max_digits=32, decimal_places=2, null=False)
    # @TODO currency
    refunded_amount = models.DecimalField(max_digits=32, decimal_places=2, default=Decimal(0), null=False)
    purchase_data = JSONField(null=False)

    @property
    def refundable_amount(self) -> Decimal:
        return self.amount - self.refunded_amount

    @property
    def is_refundable(self):
        return self.status in [Status.CONFIRMED, Status.REFUNDED]\
               and self.refundable_amount > Decimal(0)

    def save(self, *args, **kwargs):
        if not self.token:
            candidate = get_random_string(30)
            while Transaction.objects.filter(token=candidate).exists():
                candidate = get_random_string(30)
            self.token = candidate

        super().save(*args, **kwargs)

    class Meta:
        unique_together = (
            ('provider', 'provider_reference',)
            # merchant refrence is not unique for repeated transactions?
        )
        ordering = ['created_at']


class Frame(models.Model):
    transaction = models.ForeignKey(Transaction, related_name='frames', on_delete=models.PROTECT)
    created_at = models.DateTimeField(default=now)
    request = JSONField(null=True, encoder=DjangoJSONEncoder)
    response = JSONField(null=True, encoder=DjangoJSONEncoder)

    class Meta:
        ordering = ['created_at']
