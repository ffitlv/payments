class Error(Exception):
    """pass"""


class CommunicationError(Error):
    """pass"""


class ProviderError(Error):
    """pass"""


class ReponseParseError(ProviderError):
    """pass"""


class StatusError(Error):
    """pass"""


class InvalidAmountError(Error):
    pass
