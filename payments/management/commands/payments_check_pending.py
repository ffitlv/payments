from django.core.management.base import BaseCommand, CommandError
from payments.manager import manager


class Command(BaseCommand):
    help = 'Cheks pending ransactions or times them out'

    def add_arguments(self, parser):
        parser.add_argument('provider_id', nargs='?', type=str, default=None)

    def handle(self, provider_id, *args, **options):
        manager.check_pending_transactions(provider_id)
