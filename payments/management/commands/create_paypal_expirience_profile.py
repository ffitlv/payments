from django.core.management.base import BaseCommand, CommandError
from paypalrestsdk.v1.payment_experience import WebProfileCreateRequest

from payments.manager import manager


class Command(BaseCommand):
    def handle(self, *args, **options):
        # @TODO move to provider
        provider = manager.providers['paypal_express']
        request = WebProfileCreateRequest()
        request.request_body({
            "name": "default_new",
            # "presentation": {
            #     "logo_image": "https://example.com/logo_image/"
            # },
            "flow_config": {
              "user_action": "commit"
            },
            "input_fields": {
                "no_shipping": 1,
                "address_override": 1
            }
        })
        res = provider.request(request)
        print(res.result.id)
