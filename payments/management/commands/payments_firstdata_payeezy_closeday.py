from django.core.management.base import BaseCommand, CommandError
from payments.manager import manager
from payments.providers.firstdata import PayeezyProvider


class Command(BaseCommand):
    help = 'Closes business day'

    def handle(self, *args, **options):
        """
        Business day closure is necessary to close the last open batch for the merchant.
        Until batch is open, information about merchant transactions will not be sent to
        the First Data and processed by banks. Business day closure must be done every
        night at 00:00. Next batch opens with the first successful transaction.
        """
        provider = manager.providers['firstdata_payeezy']  # type: PayeezyProvider
        _request, response = provider.merchant.close_day()
        assert response['RESULT'] == 'OK'

