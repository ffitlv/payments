from django.http import HttpResponse, HttpRequest

from payments.const import Result
from payments.models import Transaction


class BaseHandler:
    def handle_response(self, request: HttpRequest, result: Result, transaction: Transaction) -> HttpResponse:
        raise NotImplementedError

    def handle_result(self, result: Result, transaction: Transaction):
        raise NotImplementedError
