# !!!!!!!!!!!!!! MOVED TO https://gitlab.ffit.lv/martins/ffpayments/-/tree/master
# !!!!!!!!!!!!!! DO NOT USE THIS
# !!!!!!!!!!!!!! DO NOT USE THIS


# Payment gateway

# Installation
Install using `pipenv`

    pipenv install git+https://bitbucket.org/ffitlv/payments/src/master/@v0.2.0rc3#egg=payments

## settings

```python
# 'payments' to your INSTALLED_APPS

INSTALLED_APPS = [  
    ...  
    'payments',  
]

# Register your PAYMENT_HANDLER, implementation details below
PAYMENT_HANDLER = 'myapp.handler.PaymentHandler'

# Set PAYMENT_BASE_PATH. This used to build callback urls
PAYMENT_BASE_PATH = 'http://payments.test'

# Choose and register PAYMENT_PROVIDERS
PAYMENT_PROVIDERS = [  
    'payments.providers.paypal.ExpressProvider',  
    'payments.providers.swedbank.HPSProvider',  
    'payments.providers.firstdata.PayeezyProvider'  
    ...  
]

#  Configure individual providers, details on options and keys below  
PAYMENT_PROVIDER_<NAME>_ = ...  
     ...  
```

## url patterns
In your `urls.py`  
```python
urlpatterns = [
    ...
    path('payments/', include('payments.urls')),
]
```

## cronjob
We should check our pending payments once a minute:

uwsgi: `unique-cron = -1 -1 -1 -1 -1 python manage.py payments_check_pending`



# Creating a payment
```python
from payments.manager import manager
from payments.exceptions import Error
from payments import dto

p = dto.Purchase(
    merchant_reference=str(order.id),  # str, unique merchant order id
    provider='swedbank_hps', # provider_id to use
    amount=order.total, # Decimal
    currency='EUR', # ISO 4217 
    description=f'My ecom order {order}',
    user_details=dto.UserDetails(
        language='lv', # ISO 639-1
        # ...
    ),
    # ...
)

try:
    # Some providers many want extra context args
    provider_redirect_url = manager.setup(p, request, **extra)
    return redirect(provider_redirect_url)

except Error as e:
    # Failed to start payment
    pass
```


# Handling payments

```python
from django.db.transaction import atomic
from django.http import HttpRequest, HttpResponse
from django.shortcuts import redirect

from payments.const import Result
from payments.handler import BaseHandler
from payments.models import Transaction


class PaymentHandler(BaseHandler):
    @atomic
    def handle_result(self, result: Result, transaction: Transaction):
        # We look up order and decide what to do with it depending on result
        order = Order.objects.select_for_update().get(pk=transaction.merchant_reference)

        if result == Result.SUCCESS:
            checkout_order(order)

        elif result in [Result.CANCELED, Result.FAILURE]:
            cancel_order(order)

        elif result in [Result.UNFINISHED, Result.STALE_CALLBACK]:
            pass

        else:
            raise RuntimeError

    @atomic
    def handle_response(self, request: HttpRequest, result: Result, transaction: Transaction) -> HttpResponse:
        merchant_ref = transaction.merchant_reference

        if result in [Result.SUCCESS, Result.STALE_CALLBACK]:
            if result == Result.SUCCESS:
                order = Order.objects.select_for_update().get(pk=merchant_ref)
                mark_paid_order(order)

            request.session['order_id'] = merchant_ref
            return redirect(reverse('cart:checkout-success'))

        elif result == Result.CANCELED:
            messages.info(request, "Your payment has been canceled.")
            return redirect(reverse('cart:checkout'))

        elif result == Result.FAILURE:
            messages.error(request, "Your payment failed.")
            return redirect(reverse('cart:checkout'))

        else:
            raise RuntimeError

```

# Providers configuration
#### [Swedbank HPS](docs/swedbank_hps.md)
#### [Paypal express](docs/paypal_express.md)
#### [FirstData Payeezy](docs/firstdata_payeezy.md)



