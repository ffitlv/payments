# Swedbank HPS
class: `payments.providers.swedbank.HPSProvider`  
id: `swedbank_hps`  
settings:  
```python
PAYMENT_SWEDBANK_HPS_CLIENT =  # required 
PAYMENT_SWEDBANK_HPS_PASSWORD = # required
# toggles endpoints and hosted page sets if omitted uses settings.DEBUG
PAYMENT_SWEDBANK_ENV = "PROD" # optional  
```
