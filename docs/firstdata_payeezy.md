# FirstData Payeezy
class: `payments.providers.firstdata.PayeezyProvider`  
id: `firstdata_payeezy`  
settings:  
```python
PAYMENT_FIRSTDATA_PAYEEZY_CLIENT = 'https://secureshop-test.firstdata.lv/ecomm/ClientHandler'
PAYMENT_FIRSTDATA_PAYEEZY_MERCHANT = 'https://secureshop-test.firstdata.lv:8443/ecomm/MerchantHandler'
# order is important here, cert/chain first then key
PAYMENT_FIRSTDATA_PAYEEZY_CERT = (
    'chain.pem',
    '1234567key_decrypted.pem'
)
```

1) Generate key, request, keystore:
```
openssl req -sha1 -newkey rsa:2048 -keyout 1234567key.pem -out 1234567req.pem -subj "/C=LV/O=shop.com/CN=1234567" -outform PEM
```

2) Sign req

3) Decrypt key
```
openssl rsa -in 1234567key.pem -out 1234567key_decrypted.pem
```
4) Covert pkcs chain
```
openssl pkcs7 -print_certs -in 1234567_certificate_chain.p7.pem -out chain.pem
```
5) Configure return URLs:  
http://localhost:8000/payments/callback/firstdata_payeezy/?result=success  
http://localhost:8000/payments/callback/firstdata_payeezy/?result=failure