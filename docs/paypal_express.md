# Paypal express
class: `payments.providers.paypal.ExpressProvider`  
id: `paypal_express`  
settings:  
```python
PAYMENT_PAYPAL_EXPRESS_CLIENT = # required
PAYMENT_PAYPAL_EXPRESS_SECRET = # required
# toggles live / sandobx env, if omitted uses settings.DEBUG 
PAYMENT_PAYPAL_EXPRESS_SANDBOX = True # optional bool
# @TODO PAYMENT_PAYPAL_EXPIRIENCE_PROFILE_ID = 
```